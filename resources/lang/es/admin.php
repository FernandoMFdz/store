<?php
return [

    "layout" => [
        "gotostore" => "Ir a la tienda",
        "signout" => "Cerrar Sesión",
        "status" => "En línea",
        "search" => "Búsqueda",

    ],
    "empty"=>[
        "ups" => "Ostras!",
        "description1" => "Nuestra base de datos dice que no existen registros",
        "description2" => "Puede que te hayas equivocado"
    ],
    "actions" => [
        "search" => "Buscar",
        "edit" => "Editar",
        "delete"=>"Borrar",
        "save" => "Guardar",
        "cancel" =>"Cancelar",
        "goback" => "Ir atrás"
    ],
    "orders" => [
        "lang" => "_es",
        "title" => "Pedidos",
        "singular" => "Pedido",
        "subtitle" => "Hoy vamos a ser productivos!",
        "status" => [
            'Select Status (Paid)'=> "Selecciona Estado (Pagado)",
            'Pending' =>'Pendiente',
            'Paid' => 'Pagado',
            'Preparing' => 'En preparación',
            'Returned' => 'Devuelto',
            'Closed' => 'Cerrado',
            'Shipped' => 'Enviado'
        ],
        "order_code" => "Código de Pedido",
        "export_invoice" => "Exportar Factura",
        "mark_preparing" =>"Voy a prepararlo!",
        "table" => [
            "reference" => "Código Referencia",
            "description" => "Descripción",
            "quantity" => "Cantidad",
            "unit_price" => "Precio Unitario",
            "total_price" => "Precio Total",
            "tax" => "IVA"
        ],

    ],
    "dashboard" => [
        "title" => "Panel de Progresión",
        "subtitle" => "Gestiona lo que realmente importa"
    ],
    "menus" => [
        "title" => "Menús",
        "subtitle" => "Porque moverse es importante",
        "here" => "Aquí",
        "table" => [
            "title"=>"Lista de Menús",
            "add"=>"Nuevo Menú",
            "name" => "Nombre",
            "description" => "Descripción",
            "slug" => "Slug",
            "unit_price" => "Precio Unitario",
            "total_price" => "Precio Total",
            "tax" => "IVA"
        ],
        "edit_title" =>"EDITAR: ",
        "create" =>[
            "title" => "Nuevo Menu",
            "name" => "Introduce el nombre del Menú",
            "description" => "Introduce una breve descripción del Menú"
        ]
    ],
    "products" => [
        "title" => "Productos",
        "subtitle" => "Gestiona el corazón de la app",
        "here" => "Aquí",
        "search" => "Nombre del Producto",
        "table" => [
            "title"=>"Lista de Productos",
            "add"=>"Nuevo Producto",
            "name" => "Nombre",
            "description" => "Descripción",
            "sell_price" => "Precio de Venta",
            "cost_price" => "Precio de Coste",
            "discounted_price" => "Precio descontado",
            "pick_line" => "Elige una Linea",
            "pick_menu" => "Elige un Menú",
        ],
        "edit_title" =>"EDITAR: ",
        "create" =>[
            "title" => "Nuevo Producto",
            "name" => "Introduce el nombre del Producto",
            "description" => "Introduce una breve descripción del Producto",
            "sell_price" => "Introduce su precio de Venta",
            "cost_price" => "Introduce su precio de Coste",
            "discounted_price" => "Introduce su precio descontado",
        ]
    ],
    "brands" => [
        "title" => "Marcas",
        "subtitle" => "LAS marcas",
        "here" => "Aquí",
        "add_line" =>"Añadir Linea",
        "search" => "Nombre de la Marca",
        "table" => [
            "title"=>"Lista de Marcas",
            "add"=>"Nueva Marca",
            "name" => "Nombre",
            "email" => "Correo",
            "phone" => "Teléfono",
            "address" => "Dirección",
            "nif_cif" => "Nif / Cif",
            "web" => "Web",
            "twitter" => "Twitter",
            "facebook" => "Facebook",
            "contact_name" => "Persona Contacto",

        ],
        "edit_title" =>"EDITAR: ",
        "create" =>[
            "title" => "Nueva Marca",
            "name" => "Introduce el nombre de la marca",
            "email" => "Introduce el correo de la marca",
            "phone" => "Introduce el Teléfono",
            "address" => "Introduce su dirección",
            "nif_cif" => "Introduce su Nif o Cif",
            "web" => "Introduce el link a su web",
            "twitter" => "Introduce el link a su twitter",
            "facebook" => "Introduce el link a su facebook",
            "contact_name" => "Introduce el nombre de la persona de contacto",
        ]
    ],
    "lines" => [
        "title" => "Líneas",
        "subtitle" => "De Marcas",
        "here" => "Aquí",
        "search" => "Nombre de la Línea",
        "table" => [
            "title"=>"Lista de Líneas",
            "add"=>"Nueva Línea",
            "name" => "Nombre",
            "description" => "Descripción",
            "brand" => "Marca"
        ],
        "edit_title" =>"EDITAR: ",
        "create" =>[
            "title" => "Nueva Línea",
            "name" => "Introduce el nombre de la linea",
            "description" => "Introduce una breve descripción de la linea",
            "brand" => "Elige una Marca"
        ]
    ],
    "users" => [
        "title" => "Usuarios",
        "lang"=>"_es",
        "subtitle" => "Controla al ganado",
        "here" => "Aquí",
        "search" => "Nombre del Usuario",
        "table" => [
            "title"=>"Lista de Usuarios",
            "add"=>"Nuevo Usuario",
            "name" => "Nombre",
            "email" => "Correo",
            "password" => "Contraseña",
            "rol"=>"Rol"
        ],
        "edit_title" =>"EDITAR: ",
        "create" =>[
            "title" => "Nuevo Usuario",
            "name" => "Introduce el nombre",
            "email" => "Introduce su Correo",
            "password" => "Introduce su Contraseña",
            "rol" => "Elige un Rol"
        ]
    ]

];