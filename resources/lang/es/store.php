<?php

return [

    'layout' => [
        'homemenu' => 'Inicio',
        'useguide' => 'Guía de Uso',
        "viewcart" => "Ver el Carrito",
        "checkout" => "Pagar",
        'signin' => 'Iniciar Sesión',
        'exam' => 'Pregunta 2 del examen!',
        'logout' => 'Cerrar Sesión',
        'adminpanel' => 'Panel de Control',
        'signup' => 'Registrarse',
        "help" => "Ayuda",
        'aboutus' => [
            'title' => "Sobre Nosotros",
            "description"=>"En Innova hemos rediseñado la forma de vender productos de belleza y cuidados con el fin de ofrecer el mejor servicio y experiencia de compra del sector."
        ],
        'member' => [
            'title' => "Miembro",
            "account"=>"Cuenta",
            "wishlist"=>"Lista de Deseados y Favoritos",
            "history"=>"Historial de Compras",
            "cart" => "Ver Carrito"
        ],
        "newsletter"=>[
            'title'=>'Suscripción',
            'email' => "Correo Electrónico",
            "subscribe" => "Suscribirme"

        ]
    ],
    "login"=>[
        "title"=>"Registrate para iniciar sesión",
        "email" => "Correo Electrónico",
        "password" => "Contraseña",
        "remember" => "Recuérdame",
        "enter" => "Entrar",
        "forgot" => "He olvidado mi contraseña",
        "new" => "Quiero registrarme",
    ],
    "register"=>[
        "title"=>"Registrate para iniciar sesión",
        "name" => "Nombre",
        "email" => "Correo Electrónico",
        "password" => "Contraseña",
        "confirm_password" => "Vuelve a escribir la contraseña",
        "remember" => "Recuérdame",
        "enter" => "Entrar",
        "terms" => "He leído y acepto los términos",
        "gotone" => "Ya tengo una cuenta",
    ],
    "cart" => [
        "title" =>"Tu carrito",
        "subtitle" => "Confirma el pedido y viajaremos por todo el mundo para entregartelo.",
        "table" => [
            "name" => "Nombre",
            "quantity" => "Cantidad",
            "price" => "Precio",
            "total" => "Total",
            "shipment_costs" => "Gastos de Envío",
        ],
        "actions" => [
            "pay" => "Pagar",
            "continue" => "Volver a la tienda"
        ],
        "item" => "Artículos",
        "error" =>  [
            "ups" => "Vaya!",
            "description1" => "Parece que no has añadido ningún producto a tu carrito de la compra",
            "subscribe" => "Vuelve a la tienda para seguir comprando cosas fabulosas!"
        ]
    ],
    "menu" => [
        "error" =>  [
            "ups" => "Vaya!",
            "description1" => "Parece que está sección aún se encuentra a medio construir",
            "subscribe" => "Suscribete a nuestra newsletter para estar actualizado!"
        ]
    ],
    "product" => [
       "points" =>[
           "title" => "Puntos",
           "description1" =>"Acumulas",
           "description2" => "puntos con esta compra"
       ],
        "quantity" => "Cantidad",
        "actions" => [
            "cart" => "Añadir a la cesta",
            "buy" => "Comprar"
        ],
        "without_iva"=>"Sin IVA",
        "brand" =>"Marca"
    ]

];