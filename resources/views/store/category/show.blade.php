@extends('layouts.app')

@section('content-styles')

    <style>
        .page-bg {
            background: url("{{url('/')}}/img/backgrounds/cover3.jpg") no-repeat center 0 fixed;
        }
        .cover-2-bg {
            background: url("{{url('/')}}/img/backgrounds/cover2.jpg") no-repeat center 0 fixed;
        }

        .breadcrumb{
            background-color: #f5f5f500;
        }
        .breadcrumb li a{
            color: #ffd3d3;
        }
    </style>
@endsection

@section('content')

    <section class="intro-block intro-page boxed-section page-bg overlay-dark-m">

        <!-- Container -->
        <div class="container">
            <!-- Section Title -->
            <div class="section-title invert-colors no-margin-b">
                <h2>{{$category->name}}</h2>
                <p class="hidden-xs">{{$category->description}}</p>
            </div>
            <!-- /Section Title -->
        </div>
        <!-- /Container -->
        <!-- Container -->
        <div class="container hidden-xs hidden-sm bread" style="margin-top: -40px;padding-bottom: 0px; padding-top: 0px;">

            <!-- Breadcrumb -->
            <ol class="breadcrumb pull-left">
                <li><a href="{{route('home')}}"><i class="ti ti-home"></i></a></li>
                <li><a href="{{route('category',$category->slug)}}">{{$category->name}}</a></li>
            </ol>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Container -->
    </section>

    <div class="col-xs-10 col-xs-offset-1">
        @if($category->products->isEmpty())
            <div class="col-xs-12" style="padding:100px 0px 200px 0px;">
                <h3 align="center"><strong>{{trans("store.category.error.ups")}}</strong></h3>
                <h4 align="center">{{trans("store.category.error.description1")}}</h4>
                <p align="center">
                    {{trans("store.category.error.subscribe")}}<br><br>

                    <h1 align="center"><i class="fa fa-level-down"></i></h1>
                    <br><br>
                </p>
            </div>
            <div class="col-xs-12">
                <div class="newsletter-block boxed-section overlay-dark-m cover-2-bg">

                    <!-- Container -->
                    <div class="container">
                        <form>
                            <!-- Row -->
                            <div class="row grid-10">
                                <!-- Col -->
                                <div class="col-sm-3 col-md-2">
                                    <span class="case-c">{{trans("store.layout.newsletter.title")}}</span>
                                </div>
                                <!-- Col -->

                                <!-- Col -->
                                <div class="col-sm-6 col-md-8">
                                    <input class="form-control" type="text" placeholder="{{trans("store.newsletter.email")}}">
                                </div>
                                <!-- Col -->

                                <!-- Col -->
                                <div class="col-sm-3 col-md-2">
                                    <button class="btn btn-block btn-color yellow-bg"><i class="icon-left fa fa-envelope"></i>{{trans("store.newsletter.subscribe")}}</button>
                                </div>
                                <!-- /Col -->

                            </div>
                            <!-- /Row -->
                        </form>
                    </div>
                    <!-- /Container -->

                </div>
            </div>
        @else
            @foreach($category->products as $product)

                    <!-- Col -->
                    <div class="col-sm-2 col-md-3 col-xs-6">
                    {{ Form::open(['route' => ['cart.store',$product->id], 'method' => 'post']) }}
                        <!-- product -->
                        <div class="product clearfix">

                            <!-- Image -->
                            <div class="image">
                                <a href="{{route('p',$product->slug)}}" class="main"><img src="{{url('/')}}/img/products/missingpic.jpg" alt=""></a>
                            </div>
                            <!-- Image -->

                            <!-- Details -->
                            <div class="details">

                                <a class="title" href="{{route('p',$product->slug)}}">{{$product->name}}</a>

                                <!-- rating -->
                                <ul class="hlinks hlinks-rating">
                                    <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                </ul>
                                <!-- /rating -->

                                <p class="desc">{{$product->details->description}}</p>
                                <!-- Price Box -->
                                <div class="price-box">
                                    <span class="price">{{$product->sell_price}}€</span>
                                </div>
                                <!-- /Price Box -->

                                <!-- buttons -->
                                <div class="btn-group">
                                    <input type="hidden" value="1" name="quantity">
                                    <button type="submit" class="btn btn-buy btn-base-hover">add to cart</button>
                                    {{ Form::close() }}
                                </div>
                                <!-- /buttons -->

                            </div>
                            <!-- /Details -->

                        </div>
                        <!-- /product -->

                    </div>
                    <!-- /Col -->

            @endforeach
        @endif
    </div>



@endsection



@section('content-script')



@endsection
