@extends('layouts.app')

@section('content')

    <!-- Page Info
      ============================================-->
    <section class="page-info-block page-info-alt  boxed-section">

        <!-- Container -->
        <div class="container cont-pad-x-15">

            <!-- Breadcrumb -->
            <ol class="breadcrumb pull-left">
                <li><a href="{{route('home')}}"><i class="ti ti-home"></i></a></li>
                <li><a href="{{route('category',$product->category->slug)}}">{{$product->category->name}}</a></li>
                <li><a href="{{route('p',$product->slug)}}">{{$product->name}}</a></li>

            </ol>
            <!-- /Breadcrumb -->


        </div>
        <!-- /Container -->

    </section>
    <!-- /Page Info Block
    ============================================-->

    <!-- Content Block
    ============================================-->
    <section class="content-block has-sidebar default-bg">
        <!-- Container -->
        <div class="container no-pad-t">

            <!-- Product Row -->
            <div class="row product-details">
                <div class="col-xs-10 col-xs-offset-1">
                    <!-- Col -->
                    <div class="col-xs-12 col-sm-12 col-md-6" style="max-width:484px; border:1px solid #C3C3C3;">
                        <a href="#"><img class="fillw" src="{{url('/')}}/img/products/missingpic.jpg" alt="" /></a>
                    </div>
                    <!-- /Col -->

                    <!-- Col -->
                    <div class="col-md-6" style="padding-right:0px;">
                        {{ Form::open(['route' => ['cart.store',$product->id], 'method' => 'post']) }}

                        <h3 class="product-title">{{$product->name}}</h3>
                        <div class="price-box">
                            <h4 class="product-price innovagradient" style="color:#FFFFFF;">{{$product->sell_price}}€</h4>
                            <em style="margin-left: 10px;color:#BCBCBC;">{{$product->sell_price}}€ {{trans("store.product.without_iva")}}</em>
                        </div>
                        <!-- Row -->
                        <div class="row grid-20" style="border:1px dotted #C3C3C3; padding:10px;">
                            <div class="row" style="margin-bottom:10px; margin-left:10px;">
                                <div class="col-xs-12 col-sm-3"><b>{{trans("store.product.brand")}}</b></div>
                                <div class="col-xs-12 col-sm-9">
                                    <a href="#">{{$product->line->brand->name}} P/N: {{$product->name}}  |  Artículo Id. {{$product->id}}</a>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px; margin-left:10px;">
                                <div class="col-xs-12 col-sm-3"><b>{{trans("store.product.points.title")}}</b></div>
                                <div class="col-xs-12 col-sm-9">
                                    <i class="fa fa-gift"></i> {{trans("store.product.points.description1")}} 50 {{trans("store.product.points.description2")}}.
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px; margin-left:10px;">
                                <div class="col-xs-12 col-sm-3"><b>{{trans("store.product.quantity")}}</b></div>
                                <div class="col-xs-12 col-sm-4">
                                    {{--<button id="delCantidad" style="margin-right: -5px;"><i class="fa fa-minus"></i></button>--}}
                                    <input name="quantity" id="cantidad" value="1" style="width: 50px;text-align: center;">
                                    {{--<button id="addCantidad" style="margin-left: -5px;"><i class="fa fa-plus"></i></button>--}}
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px; margin-left:10px;">
                                <div class="col-xs-12 col-sm-3"><b>Descripción</b></div>
                                <div class="col-xs-12 col-sm-9">
                                    {{$product->details->description}}
                                </div>
                            </div>

                        </div>

                        <div class="row grid-20" style="border:1px dotted #C3C3C3; padding:5px; margin-top:7px;">
                            <div class="col-xs-6 col-sm-6"><button name="cart" type="submit" class="btn btn-cesta"><span class="hidden-xs">{{trans("store.product.actions.cart")}}</div>
                            <div class="col-xs-6 col-sm-6"><button name="buy" type="submit" class="btn btn-buy"><i class="fa fa-shopping-cart "></i> {{trans("store.product.actions.buy")}}</button></div>
                            {{ Form::close() }}
                        </div>

                        <div class="row grid-20" style="border:1px dotted #C3C3C3; padding:5px; margin-top:7px;">

                            <div class="col-xs-6 col-sm-6">
                                <!-- hlinks -->
                                <ul style="margin-top:5px;margin-bottom:5px;" class="page-links hlinks hlinks-icons color-icons-borders color-icons-bg-hovered">
                                    <li><a href="#"><i style="padding:10px; font-size:20px;" class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i style="padding:10px; font-size:20px;" class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i style="padding:10px; font-size:20px;" class="fa fa-rss"></i></a></li>
                                </ul>
                                <!-- /hlinks -->
                            </div>

                            <div class="col-xs-6 col-sm-6" style="text-align:right;">
                                pagar con <img style="width:30%; height:auto; " src="{{url('/')}}/img/cards/paypal.png">
                            </div>
                        </div>

                        <div class="row grid-20" style="border:1px dotted #C3C3C3; padding:5px; margin-top:7px;">
                            <div class="row" style="margin-bottom:30px;">
                                <div class="col-xs-7 col-sm-7">
                                    <b style="font-size:20px; padding:10px;">{{$product->name}} opiniones</b>
                                </div>
                                <div class="col-xs-5 col-sm-5">
                                    95% Positivas | 5% negativas
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12">

                                    <div class="clearfix">
                                        <!-- Avatar -->
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar">
                                                <img alt="avatar" src="{{url('/')}}/img/avatar.png">
                                            </div>
                                            <!-- /Avatar -->


                                            <!-- Author -->
                                            <div class="author clearfix">
                                                <a class="username" href="#">Fernando Meseguer</a>
                                                <span class="date">22/09/2016</span>
                                            </div>
                                            <!-- /Author -->

                                            <!-- rating -->
                                            <ul class="hlinks hlinks-rating">
                                                <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="icon fa fa-star"></i></a></li>

                                            </ul>
                                            <!-- /rating -->
                                        </div>
                                        <div class="col-xs-12 col-sm-10">
                                            <!-- Comment -->
                                            <p class="comment">Que bien luce el panel de productos de Innova! Wow! dan ganas de comprar y todo! Que colores más chulos y que buenas sensaciones produce verlo. Uala! también se adapta genial al teléfono!</p>
                                            <!-- /Comment -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->


                </div>
                <!-- /Col -->

            </div>
            <!-- /Product Row -->
        </div>
        <div class="col-xs-8 col-xs-offset-2">


        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-tabs-line-bottom line-pcolor nav-tabs-center case-u" role="tablist">
            <li class="active"><a href="#tab-descripcion" data-toggle="tab">Descripción</a></li>
            <!-- <li><a href="#tab-rebajados" data-toggle="tab">Top Ventas</a></li> -->
            <li><a href="#tab-comments" data-toggle="tab">Comentarios</a></li>
        </ul>
        <!-- /Nav Tabs -->
        <!-- Tab panes -->
        <div class="tab-content tab-no-borders">

            <!-- Tab Latest -->
            <div class="tab-pane active" id="tab-descripcion">

                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-9" style="border-right:1px solid #B6B6B6;">
                            oojojojo
                        </div>
                        <div class="col-lg-3">
                            <!-- Side Widget -->
                            <div class="side-widget">

                                <h5 class="boxed-title">similar products</h5>

                                <!-- Slider Wrapper -->
                                <div class="side-products-slider bx-controls-above-right product-no-margin">

                                    <!-- BxSlider -->
                                    <div class="bxslider" data-call="bxslider" data-options="{pager:false,auto:true,pause: 2000}">

                                        <!-- Slide -->
                                        <div class="slide">
                                            <!-- product -->
                                            <div class="product clearfix">

                                                <!-- Image -->
                                                <div class="image">
                                                    <a href="product.html" class="main"><img src="{{url('/')}}/img/products/product7.jpg" alt=""></a>
                                                    <ul class="additional">
                                                        <li><a href="{{url('/')}}/img/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="{{url('/')}}/img/products/product7.jpg" alt=""></a></li>
                                                        <li><a href="{{url('/')}}/img/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="{{url('/')}}/img/products/product7.jpg" alt=""></a></li>
                                                        <li><a href="{{url('/')}}/img/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="{{url('/')}}/img/products/product7.jpg" alt=""></a></li>
                                                        <li><a href="{{url('/')}}/img/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="{{url('/')}}/img/products/product7.jpg" alt=""></a></li>
                                                    </ul>
                                                </div>
                                                <!-- Image -->


                                                <span class="label label-sale">sale</span>

                                                <!-- Details -->
                                                <div class="details">

                                                    <a class="title" href="product.html">One pice business suit</a>

                                                    <!-- rating -->
                                                    <ul class="hlinks hlinks-rating">
                                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                    </ul>
                                                    <!-- /rating -->

                                                    <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>

                                                    <!-- Price Box -->
                                                    <div class="price-box">
                                                        <span class="price price-old">$3350</span><span class="price">$2050</span>
                                                    </div>
                                                    <!-- /Price Box -->

                                                    <!-- buttons -->
                                                    <div class="btn-group">
                                                        <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>
                                                        <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                                                    </div>
                                                    <!-- /buttons -->

                                                </div>
                                                <!-- /Details -->

                                            </div>
                                            <!-- /product -->
                                        </div>
                                        <!-- /Slide -->

                                        <!-- Slide -->
                                        <div class="slide">
                                            <!-- product -->
                                            <div class="product clearfix">

                                                <!-- Image -->
                                                <div class="image">
                                                    <a href="product.html" class="main"><img src="{{url('/')}}/img/products/product8.jpg" alt=""></a>
                                                    <ul class="additional">
                                                        <li><a href="{{url('/')}}/img/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="{{url('/')}}/img/products/product8.jpg" alt=""></a></li>
                                                        <li><a href="{{url('/')}}/img/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="{{url('/')}}/img/products/product8.jpg" alt=""></a></li>
                                                        <li><a href="{{url('/')}}/img/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="{{url('/')}}/img/products/product8.jpg" alt=""></a></li>
                                                        <li><a href="{{url('/')}}/img/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="{{url('/')}}/img/products/product8.jpg" alt=""></a></li>
                                                    </ul>
                                                </div>
                                                <!-- Image -->

                                                <!-- Details -->
                                                <div class="details">

                                                    <a class="title" href="product.html">Premium fur jacket</a>

                                                    <!-- rating -->
                                                    <ul class="hlinks hlinks-rating">
                                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                                    </ul>
                                                    <!-- /rating -->

                                                    <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>

                                                    <!-- Price Box -->
                                                    <div class="price-box">
                                                        <span class="price">$8400</span>
                                                    </div>
                                                    <!-- /Price Box -->

                                                    <!-- buttons -->
                                                    <div class="btn-group">
                                                        <a class="btn btn-outline btn-base-hover" href="product.html">add to cart</a>
                                                        <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                                                    </div>
                                                    <!-- /buttons -->

                                                </div>
                                                <!-- /Details -->

                                            </div>
                                            <!-- /product -->
                                        </div>
                                        <!-- /Slide -->

                                    </div>
                                    <!-- /BxSlider -->

                                </div>
                                <!-- /Slider Wrapper -->

                            </div>
                            <!-- /Side Widget -->
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>
            <!-- /Tab Latest -->

            <!-- Tab Featured -->
            <div class="tab-pane" id="tab-comments">

                <!-- Row -->
                <div class="row">
                    <!-- Col -->
                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                        Valoración Innova: 8,9
                    </div>
                    <!-- /Col -->
                    <!-- Col -->
                    <div class="col-lg-9 col-sm-9 col-md-9 col-xs-9" style="border-left:1px solid #D9D9D9;">

                        <!-- Comments List -->
                        <ol class="comments-list">

                            <li class="clearfix">
                                <!-- Avatar -->
                                <div class="avatar">
                                    <img alt="avatar" src="{{url('/')}}/img/avatar.png">
                                </div>
                                <!-- /Avatar -->

                                <div class="comment-box clearfix">
                                    <!-- Author -->
                                    <div class="author clearfix">
                                        <a class="username" href="#">Sean Carter</a>
                                        <span class="date">Feb 16th, 2015 10:14 am</span>
                                    </div>
                                    <!-- /Author -->

                                    <!-- rating -->
                                    <ul class="hlinks hlinks-rating">
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    </ul>
                                    <!-- /rating -->

                                    <!-- Comment -->
                                    <p class="comment">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words</p>
                                    <!-- /Comment -->
                                </div>

                            </li>

                            <li class="clearfix">
                                <!-- Avatar -->
                                <div class="avatar">
                                    <img alt="avatar" src="{{url('/')}}/img/avatar.png">
                                </div>
                                <!-- /Avatar -->

                                <div class="comment-box clearfix">
                                    <!-- Author -->
                                    <div class="author clearfix">
                                        <a class="username" href="#">Mary Jane</a>
                                        <span class="date">Feb 11th, 2015 4:00 am</span>
                                    </div>
                                    <!-- /Author -->

                                    <!-- rating -->
                                    <ul class="hlinks hlinks-rating">
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    </ul>
                                    <!-- /rating -->

                                    <!-- Comment -->
                                    <p class="comment">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words</p>
                                    <!-- /Comment -->
                                </div>

                            </li>

                            <li class="clearfix">
                                <!-- Avatar -->
                                <div class="avatar">
                                    <img alt="avatar" src="{{url('/')}}/img/avatar.png">
                                </div>
                                <!-- /Avatar -->

                                <div class="comment-box clearfix">
                                    <!-- Author -->
                                    <div class="author clearfix">
                                        <a class="username" href="#">John Doe</a>
                                        <span class="date">Feb 11th, 2015 12:25 am</span>
                                    </div>
                                    <!-- /Author -->

                                    <!-- rating -->
                                    <ul class="hlinks hlinks-rating">
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                                    </ul>
                                    <!-- /rating -->

                                    <!-- Comment -->
                                    <p class="comment">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words</p>
                                    <!-- /Comment -->
                                </div>

                            </li>

                        </ol>
                        <!-- Comments List / END -->

                    </div>
                    <!-- /Col -->

                </div>
                <!-- /Row -->

            </div>
            <!-- /Tab Featured -->


        </div>
        <!-- /Tab Panes -->
        </div>
        <br>
@endsection