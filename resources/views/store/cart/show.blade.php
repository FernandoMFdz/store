@extends('layouts.app')

@section('content-styles')

    <style>
        .page-bg {
            background: url("{{url('/')}}/img/backgrounds/cover1.jpg") no-repeat center 0 fixed;
        }

        .cover-2-bg{
            background: url("{{url('/')}}/img/backgrounds/cover2.jpg") no-repeat center 0 fixed;
        }
    </style>

@endsection
@section('content')


    <!-- Intro Block
      ============================================-->
    <!-- /Page Info  Block
    ============================================-->
    <section class="intro-block intro-page boxed-section page-bg overlay-dark-m" style="margin-top:-10px;">

        <!-- Container -->
        <div class="container">
            <!-- Section Title -->
            <div class="section-title invert-colors no-margin-b">
                <h2>{{trans("store.cart.title")}}</h2>
                <p style="margin-bottom:30px;">{{trans("store.cart.subtitle")}}</p>
            </div>
            <!-- /Section Title -->
        </div>
        <!-- /Container -->

    </section>
    <!-- /Intro Block
    ============================================-->

    <!-- Content Block
      ============================================-->
    <section class="content-block default-bg">

        <!-- Container -->
        <div class="container cont-pad-y-sm">

            <!-- Row -->
            <div class="row">
                @if($order_id === null)
                    @include('store.cart.partials.empty')

                @else

            @if($order->details->count() == 0)
                @include('store.cart.partials.empty')
            @else
                <!-- Main Col -->
                <div class="main-col col-md-9 mgb-30-xs">

                    <section class="content-block default-bg" style="margin-top: -85px;">

                        <!-- Container -->
                        <div class="container cont-pad-t-sm">

                            <!-- Cart -->
                            <div class="cart" id="tcart">

                                <!-- Cart Contents -->
                                <table class="cart-contents">
                                    <thead>
                                    <tr>
                                        <th class="hidden-xs"></th>
                                        <th>{{trans("store.cart.table.name")}}</th>
                                        <th>{{trans("store.cart.table.quantity")}}</th>
                                        <th class="hidden-xs">{{trans("store.cart.table.price")}}</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order->details as $detail)
                                    <tr>
                                        <td class="image hidden-xs"><img style="width: 80%; height: auto;margin-left: 23px;" src="{{url('/')}}/img/products/missingpic.jpg" alt="product"/></td>
                                        <td>{{$detail->product->name}}</td>
                                        <td>{{$detail->quantity}}</td>
                                        <td>{{$detail->product->sell_price}}</td>
                                        <td>{{$detail->quantity*$detail->product->sell_price}}</td>
                                        <td>
                                            {{Form::open(['method' => 'DELETE', 'route' => ['cart.detail.delete',$detail->id]])}}

                                            <button type="submit" style="width: 40px;height: 27px;font-size: 16px;padding: 0px;"
                                                    class="btn btn-cesta"
                                                    href="#" name="delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            {{Form::close()}}
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>

                            </table>
                            <!-- /Cart Contents -->

                            </div>
                            <!-- /#TCart -->
                        </div>
                        <!-- /Cart -->
                    </section>
                </div>
                <!-- /Main Col -->

    <!-- Side Col -->
    <div class="side-col col-md-3" style=" margin-top: -65px;">

        <!-- Side Widget -->
        <div class="order-summary" style="background-color: white">
            <div id="tsum">
                <table>
                    <tbody>
                    <tr>
                        <td>{{trans("store.cart.item")}} ({{$order->details->count()}}) </td>
                        <td class="price">{{$order->total}} €</td>
                    </tr>
                    <tr>
                        <td>{{trans("store.cart.table.shipment_costs")}}</td>
                        <td class="price"><span class="success">3,00 €</span></td>
                    </tr>
                    <tr class="total">
                        <td> Total </td>
                        <td class="price">{{$order->total + 3}} €</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            {{Form::open(['method' => 'PUT', 'route' => ['cart.pay',$order->id]])}}
                <button type="submit" class="btn btn-cesta checkout" href="#"><i class="icon-left fa fa-shopping-cart"></i>{{trans("store.cart.actions.pay")}}</button>
            {{Form::close()}}
            <a class="btn btn-buy checkout" href="{{route('home')}}"><i class="icon-left fa fa-arrow-left"></i>{{trans("store.cart.actions.continue")}}</a>
        </div>
        <!-- /Side Widget -->


    </div>
    <!-- /Side Col -->
                @endif
                    @endif
    </div>
    <!-- /Row -->
    </div>
    <!-- /Container -->

    </section>
    <!-- /Content Block
    ============================================-->

    <!-- Newsletter Block
    ============================================ -->
    <div class="newsletter-block boxed-section overlay-dark-m cover-2-bg">

        <!-- Container -->
        <div class="container">
            <form>
                <!-- Row -->
                <div class="row grid-10">
                    <!-- Col -->
                    <div class="col-sm-3 col-md-2">
                        <span class="case-c">{{trans("store.layout.newsletter.title")}}</span>
                    </div>
                    <!-- Col -->

                    <!-- Col -->
                    <div class="col-sm-6 col-md-8">
                        <input class="form-control" type="text" placeholder="{{trans("store.newsletter.email")}}">
                    </div>
                    <!-- Col -->

                    <!-- Col -->
                    <div class="col-sm-3 col-md-2">
                        <button class="btn btn-block btn-color yellow-bg"><i class="icon-left fa fa-envelope"></i>{{trans("store.newsletter.subscribe")}}</button>
                    </div>
                    <!-- /Col -->

                </div>
                <!-- /Row -->
            </form>
        </div>
        <!-- /Container -->

    </div>
    <!-- /Newsletter Block
    =================================================== -->


@endsection