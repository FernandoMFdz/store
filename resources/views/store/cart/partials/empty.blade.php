<div class="col-xs-12" style="padding:100px 0px 200px 0px;">
    <h3 align="center"><strong>{{trans("store.cart.error.ups")}}</strong></h3>
    <h4 align="center"> {{trans("store.cart.error.description1")}}</h4>
    <p align="center">
        {{trans("store.cart.error.subscribe")}}<br><br>
    <div class="col-xs-4 col-xs-offset-4">
        <a class="btn btn-buy checkout" href="{{route('home')}}"><i class="icon-left fa fa-arrow-left"></i>{{trans("store.cart.actions.continue")}}</a>
    </div>
    <br><br>
    </p>
</div>