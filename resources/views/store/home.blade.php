@extends('layouts.app')

@section('content-styles')
    <style>

        .img-main{ width: auto;height:70%;}
        .alert-danger{
             position: relative;
             max-width: 85%;
             float: none;
             padding-bottom: 12px;
             top: 0px;
             margin-bottom: 0px;
            background-color: #f2dede;
            border-color: #dcdcdc;
            border-top: none;
            color: #e7214c;
        }

        .alert-success{
            position: relative;
            max-width: 85%;
            float: none;
            padding-bottom: 12px;
            top: 0px;
            margin-bottom: 0px;
        }

    </style>
@endsection

@section('content')
    <ul class="nav nav-tabs nav-tabs-line-bottom line-pcolor nav-tabs-center case-u" role="tablist">
        <li class="active"><a href="#inicio" data-toggle="tab">{{trans("store.layout.homemenu")}}</a></li>
        <li><a href="#exam" data-toggle="tab">{{trans("store.layout.exam")}}</a></li>
        <li><a href="#guide" data-toggle="tab">{{trans("store.layout.useguide")}}</a></li>
    </ul>
    <br>

    <!-- Content Block
      ============================================ -->
    <div class="content-block">

        <!-- Container -->
        <div class="container no-pad-t">

            <!-- Product Tabs -->
            <div class="product-tabs">
                
                <!-- Tab panes -->
                <div class="tab-content tab-no-borders">

                    <!-- Tab Latest -->
                    <div class="tab-pane active" id="inicio">
                        <div class="intro-block mgb-20">

                            <!-- Container -->
                            <div class="container-slider">

                                <div class="intro-slider">
                                    <div class="owl-carousel clg-1">

                                        <!-- Slide -->
                                        <div class="slide">
                                            <a href="#guide" data-toggle="tab"> <img class="img-main" src="{{url('/')}}/img/slides/slide4-4.png" alt=""/></a><!-- slider image + background -->
                                        </div>
                                        <!-- /Slide -->
                                        <!-- Slide -->
                                        <div class="slide">
                                            <img class="img-main" src="{{url('/')}}/img/slides/slide2-1.png" alt=""/><!-- slider image + background -->
                                            <!-- Text -->
                                            <div class="text">
                                                <div class="vcenter">
                                                    <div class="vcenter-this text-block">
                                                        <h1 class="bx-layer" data-anim="bounceInDown" data-dur="1000" data-delay="100">Guía de Uso</h1><br/>
                                                        <p class="bx-layer hidden-xs" data-anim="bounceInRight" data-dur="1000" data-delay="500">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet consectetur veli..</p>
                                                        <a href="#guide" data-toggle="tab" class="btn btn-white bx-layer hidden-xs" data-anim="bounceInUp" data-dur="1000" data-delay="1000">Ir a Guia de Uso</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Text -->
                                        </div>
                                        <!-- /Slide -->
                                        <!-- Slide -->
                                        <div class="slide">
                                            <img class="img-main" src="{{url('/')}}/img/slides/slide3-1.png" alt=""/><!-- slider image + background -->
                                            <!-- Text -->
                                            <div class="text">
                                                <div class="vcenter">
                                                    <div class="vcenter-this text-block">
                                                        <h1 class="bx-layer" data-anim="bounceInDown" data-dur="1000" data-delay="100">Guía de Uso</h1><br/>
                                                        <a href="#guide" data-toggle="tab" class="btn btn-buy bx-layer hidden-xs" data-anim="bounceInUp" data-dur="1000" data-delay="1000">Ir a Guia de Uso</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Text -->
                                        </div>
                                        <!-- /Slide -->
                                    </div>
                                </div>

                            </div>
                            <!-- Slider Wrapper -->

                        </div>
                        <!-- /Container -->



                        <!-- Row -->
                            <div class="row">
                                <!-- /Content Block
   ============================================ -->
                            @foreach($categories as $category)

                                <!-- Content Block
      ============================================ -->
                                    <div class="content-block" >

                                        <!-- Container -->
                                        <div class="container cont-md">

                                            <!-- Section Title -->
                                            <div class="section-title line-pcolor" >
                                                <h2><a href="{{route('category',$category->slug)}}" style="color:black">{{$category->name}}</a></h2>
                                                <p style="margin-bottom:-30px;">{{$category->description}}</p>
                                            </div>
                                            <!-- /Section Title -->

                                            <!-- Slider Wrapper -->
                                            <div class="brand-slider">

                                                <!-- BxSlider -->
                                                <div class="bxslider" data-call="bxslider" data-options="{pager:false, mode:'horizontal',auto:true,pause:5000, slideMargin:20}" data-breaks="[{screen:0, slides:4}]">
                                                @foreach($category->products()->paginate(5) as $product)
                                                    <!-- Slide -->
                                                        <div class="slide">
                                                            <!-- Col -->
                                                            <div class="col-lg-12 col-sm-12 col-md-2 col-xs-12">
                                                            {{ Form::open(['route' => ['cart.store',$product->id], 'method' => 'post']) }}

                                                            <!-- product -->
                                                                <div class="product clearfix">
                                                                    <!-- Image -->
                                                                        <div class="image">
                                                                            <a  href="{{route('p',$product->slug)}}" class="main"><img src="{{url('/')}}/img/products/missingpic.jpg" alt=""></a>
                                                                        </div>

                                                                    <!-- Image -->
                                                                    <!-- Details -->
                                                                    <div class="details">

                                                                        <a class="title"  href="{{route('p',$product->slug)}}">{{$product->name}}</a>
                                                                        <p class="desc">{{$product->details->description}}</p>
                                                                        <!-- Price Box -->
                                                                        <div class="price-box">

                                                                            {{--<span class="price price-old ">4€</span>--}}
                                                                            <span class="price">{{$product->sell_price}}€</span>

                                                                        </div>
                                                                        <!-- /Price Box -->
                                                                        <!-- buttons -->
                                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 vcenter">
                                                                            <div class="btn-group">

                                                                                <input type="hidden" value="1" name="quantity">
                                                                                <button class="btn btn-buy btn-carrito"><i class="icon fa fa-shopping-cart"></i></button>
                                                                                {{ Form::close() }}
                                                                            </div>
                                                                            <!-- /buttons -->
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Details -->

                                                                </div>
                                                                <!-- /product -->

                                                            </div>
                                                            <!-- /Col -->
                                                        </div>
                                                        <!-- /Slide -->
                                                    @endforeach
                                                </div>
                                                <!-- /BxSlider -->

                                            </div>
                                            <!-- Slider Wrapper -->
                                        </div>
                                        <!-- /Container -->

                                    </div>
                                    <!-- Content Block
                                    ============================================ -->

                                @endforeach
                            </div>
                            <!-- /Row -->
                    </div>
                    <!-- /Tab Latest -->

                    <!-- Tab Trending -->
                    <div class="tab-pane" id="guide">
                        <div class="container">
                            <div class="col-md-3">
                                <ul class="nav nav-pills nav-stacked" style="width: 100%">
                                    <li class="active"><a data-toggle="pill" href="#home">De qué va la aplicación</a></li>
                                    <li><a data-toggle="pill" href="#menu1">Roles</a></li>
                                    <li><a data-toggle="pill" href="#menu2">Acceso</a></li>
                                    <li><a data-toggle="pill" href="#menu5">Tests Unitarios</a></li>
                                    <li><a data-toggle="pill" href="#menu3">Cambios del PDF entregado a la Entrega Final</a></li>
                                    <li><a data-toggle="pill" href="#menu4">Vistas y plantilla usada</a></li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <h2>De qué va la aplicación</h2>
                                        <hr>
                                        <br>
                                        <p>La aplicación consta fundamentalmente de 2 partes:</p>
                                        <p>
                                            <br>
                                            <div class="row grid-20" style="border:1px solid #C3C3C3; padding:5px; margin-top:7px;">
                                                <div class="col-xs-6 col-sm-6"><a href="#la-tienda" class="btn btn-buy" style="padding: 40px 40px;"><span class="hidden-xs"></span><i class=" fa fa-shopping-bag"></i> La Tienda</a></div>
                                                <div class="col-xs-6 col-sm-6"><a href="#control-panel" class="btn btn-cesta" style="padding: 40px 40px;"><i class="fa fa-bar-chart "></i> Panel de Control</a></div>
                                            </div>
                                        </p>
                                        <h3 id="control-panel">Panel de Control</h3>
                                        <p>
                                            El panel de control de esta aplicacion es un lugar reservado a aquellos usuarios que tengan el permiso necesario
                                            para entrar.
                                        </p>
                                        <p>
                                            <br>
                                            En esta sección de la aplicación se encuentran las siguientes características:
                                            <br>


                                            <ul>
                                            <li><b>Dashboard</b>
                                                <ul>
                                                    <li>Ver el número de pedidos que se encuentran en el carrito de la compra</li>
                                                    <li>Ver el número de pedidos que ya se han pagado</li>
                                                    <li>Ver el importe total de todos los pedidos pagados de este mes</li>
                                                    <li>Ver el importe total de todos los pedidos que se encuentran en el carrito este mes</li>
                                                    <li>Ver el número de usuarios registrados nuevos este mes</li>
                                                </ul>

                                            </li>
                                            <li>Gestión de <b>Menús</b>
                                                <ul>
                                                    <li>CRUD</li>
                                                </ul>
                                            </li>

                                            <li>Gestión de <b>Productos</b>
                                                <ul>
                                                    <li>CRUD</li>
                                                    <li>Buscador</li>
                                                </ul>
                                            </li>

                                            <li>Gestión de <b>Marcas</b>
                                                <ul>
                                                    <li>CRUD</li>
                                                    <li>Buscador</li>
                                                </ul>
                                            </li>

                                            <li>Gestión de <b>Lineas de productos</b> de Marcas
                                                <ul>
                                                    <li>CRUD</li>
                                                    <li>Buscador</li>
                                                </ul>
                                            </li>
                                            <li>Gestión de <b>Usuarios</b>
                                                <ul>
                                                    <li>CRUD</li>
                                                    <li>Buscador</li>
                                                </ul>
                                            </li>

                                            <li>Gestión de <b>Pedidos</b>
                                                <ul>
                                                    <li>Ver Pedidos Pendientes (los que aun se encuentran en el carrito)</li>
                                                    <li>Ver Pedidos Pagados (no hay conexión a paypal aún)</li>
                                                    <li>Buscador de pedidos por Código de Pedido y por Estado del Pedido</li>
                                                    <li>Podremos <b>Generar Facturas (PDF) de los Pedidos</b> (Beta)</li>
                                                </ul>
                                            </li>

                                            </ul>
                                        <div class="alert alert-danger">
                                            <strong>Ojo!</strong> Las anteriores funcionalidades no aparecerán en el panel de control si no se
                                           tiene el permiso necesario. Si se intenta acceder ingresando la URL y no se tiene el permiso devolverá
                                            un error 404. Para probar todas las funcionalidades recomiendo usar el rol de <b>Programador</b>
                                        </div>
                                        </p>

                                        <h2 id="la-tienda">La tienda</h2>
                                        <p>
                                            Casi el 80% de la función del panel de control es generar el contenido
                                            necesario para que la tienda funcione:
                                        </p>
                                        <p>
                                            <br>
                                            Esta es la sección principal de la aplicacion, la expuesta al público y que
                                            tiene acceso todo el mundo. Presenta las siguientes características:
                                            <br>


                                        <ul>
                                            <li><b>Sistema de Navegación por menús</b>

                                                <ul>
                                                    <li>
                                                        Dependiendo de los menús que hayan creados en el panel de control aparecerán
                                                        en la tienda para poder ir navegando por los menús (usando el slug) para ver
                                                        los productos que se encuentran vinculados a este <br><br>
                                                        En cierta forma funcionan como categorías de producto.
                                                    </li>
                                                </ul>
                                            </li>

                                            <li><b>Vista de Producto Individual</b>

                                                <ul>
                                                    <li>
                                                        Dependiendo de los productos que hayan creados en el panel de
                                                        control aparecerán tanto en el inicio como en la vista individual de
                                                        menús una serie de productos. Al clicar sobre ese producto me podrán ver
                                                        los detalles del mismo al igual que añadirlo a la cesta de la compra.
                                                    </li>

                                                </ul>
                                            </li>

                                            <li><b>Sistema de Carrito de la Compra</b>

                                                <ul>
                                                    <li>Añadir un producto y su cantidad al carrito</li>
                                                    <li>Eliminar el producto del carrito desde el layout</li>
                                                    <li>Eliminar el producto del carrito la vista individual del carrito (/cart)</li>
                                                    <li>Botón de pagar. Aquí conectaría con paypal y le pasaría los datos del pedido. De momento solo cambia el estado del pedido a 'Paid'</li>

                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="alert alert-warning">
                                            Para interactuar con el carrito de la compra es necesario autenticarse, no importa el rol que tenga el usuario.
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <h2>Roles</h2>
                                        <hr>
                                        <br>
                                        <h3>Roles en el Panel de Control</h3>
                                        <br>
                                        <br>
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Nivel de Acceso</th>
                                                    <th>Rol</th>
                                                    <th>Acceso al Dashboard</th>
                                                    <th>Gestion de Pedidos</th>
                                                    <th>Gestion de Menús</th>
                                                    <th>Gestion de Marcas</th>
                                                    <th>Gestion de Lineas</th>
                                                    <th>Gestion de Productos</th>
                                                    <th>Gestion de Usuarios</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>6</td>
                                                    <td>Programador</td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>Director</td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td><i class="fa fa-check"></i></td>

                                                </tr>

                                                <tr>
                                                    <td>4</td>
                                                    <td>Administrador</td>
                                                    <td>-</td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td><i class="fa fa-check"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Personal de la Tienda</td>
                                                    <td>-</td>
                                                    <td><i class="fa fa-check"></i></td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Personal del Salón de Belleza</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                </tr>

                                                <tr>
                                                    <td>1</td>
                                                    <td>Cliente de la tienda</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <h3>Roles en La Tienda</h3>
                                        <br>
                                        <br>
                                        <div class="alert alert-success">
                                            <b>NO</b> hay roles en la tienda, todos los usuarios pueden entrar en ella y hacer pedidos.
                                        </div>

                                    </div>
                                    <div id="menu2" class="tab-pane fade">
                                        <h2>Acceso</h2>
                                        <hr>
                                        <br>
                                        <h3>Acceso al Panel de Control</h3>
                                        <br>
                                        <p>Estos son los roles que se crean por defecto cuando se despliegan los seeders</p>
                                        <br>
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Rol</th>
                                                    <th>Correo</th>
                                                    <th>Contraseña</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Programador</td>
                                                    <td>cmabris@gmail.com</td>
                                                    <td>123456</td>

                                                </tr>
                                                <tr>
                                                    <td>Director</td>
                                                    <td>director@gmail.com</td>
                                                    <td>123456</td>

                                                </tr>

                                                <tr>
                                                    <td>Administrador</td>
                                                    <td>administrador@gmail.com</td>
                                                    <td>123456</td>
                                                </tr>
                                                <tr>
                                                    <td>Personal Tienda</td>
                                                    <td>personaltienda@gmail.com</td>
                                                    <td>123456</td>
                                                </tr>
                                                <tr>
                                                    <td>Personal Salon</td>
                                                    <td>personalsalon@gmail.com</td>
                                                    <td>123456</td>
                                                </tr>

                                                <tr>
                                                    <td>Cliente</td>
                                                    <td>cliente@gmail.com</td>
                                                    <td>123456</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="menu3" class="tab-pane fade">
                                        <h2>Cambios del PDF entregado a la Entrega Final</h2>
                                        <hr>
                                        <br>
                                        <a href="{{url('/')}}/pdf/prototipo.pdf" download class="btn btn-success">Descargar el PDF presentado</a>
                                        <br>
                                        <br>
                                        <br>
                                        <p>
                                            <ul>

                                            <li>Reemplazo de CRUD de Categorías por CRUD de Líneas de Productos</li>
                                            <li>Reemplazo el Sistema de Comentarios por Sistema de Carrito de la Compra</li>
                                            <li>Reemplazo el Buscador de productos general (en la tienda) por por el Sistema de Gestión de Pedidos </li>
                                            <li>Cambios menores en los Roles debido a los cambios anteriores </li>
                                        </ul>

                                        </p>
                                    </div>

                                    <div id="menu4" class="tab-pane fade">
                                        <h2>Vistas y plantilla usada</h2>
                                        <hr>
                                        <br>
                                        <br>
                                        <br>
                                        <h3>Panel de Administración</h3>
                                        <p>
                                            He usado <a class="btn btn-success" href="https://almsaeedstudio.com">AdminLTE</a> y he retocado un poco los colores.
                                        </p>

                                        <hr>
                                        <br>
                                        <h3>La tienda</h3>
                                        <p>
                                            He usado <a class="btn btn-buy" href="https://wrapbootstrap.com/theme/helena-html-opencart-store-theme-WB049G842">Helena </a> una plantilla de pago personalizada a fondo para el proyecto
                                        </p>
                                    </div>

                                    <div id="menu5" class="tab-pane fade">
                                        <h2>Tests Unitarios</h2>
                                        <hr>
                                        <br>
                                        <div class="alert alert-success">La mayor parte del desarrollo de la aplicación ha sido guiada por pruebas unitarias. </div>
                                        <p>
                                            Al ejecutar las pruebas a día 25 de Febrero de 2017 usando PHPUnit, todas pasan satisfactoriamente.
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Tab Trending -->

                    <!-- Tab Trending -->
                    <div class="tab-pane" id="exam">
                        <div class="container">
                            <div class="col-md-3">
                                <ul class="nav nav-pills nav-stacked" style="width: 100%">
                                    <li class="active"><a data-toggle="pill" href="#home">Respuesta</a></li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <h2>La respuesta</h2>
                                        <hr>
                                        <br>
                                        <ul>
                                            <li>

                                                <h4>En primer lugar</h4>
                                                <p>
                                                    Me la descargue tal cual viene de este enlace <a href="https://almsaeedstudio.com/download/AdminLTE-master" class="btn btn-success">Descargar AdminLTE</a><br>
                                                    Después la descomprimí y metí el contenido del .zip a la carpeta public para empezar a trabar con ella.<br>
                                                    <sub>Es posible que meterla en la carpeta public no sea lo más recomendable y debería de haberse incluido en la carpeta RESOURCES > ASSETS</sub>
                                                </p>



                                            </li>

                                            <li>
                                                <p>
                                                    <h4>En Segundo lugar</h4>
                                                <br>
                                                Fui a PUBLIC > CP y abrí el archivo starter.html, separé el código en 3 partes fundamentales para su correcto funcionamiento
                                                    <ol>
                                                        <li>El estilo</li>
                                                        <li>El contenido</li>
                                                        <li>Los scripts de JavasCript</li>
                                                    </ol>

                                                <br>
                                                Creé un layout en RESOURCES > VIEWS > LAYOUT > ADMIN llamado app.blade.php y creé la carpeta de parciales donde puse los componentes principales:
                                                <ol>
                                                    <li>Los estilos (formados con la url dada por laravel)</li>
                                                    <li>El menú de la app (para trabajar más comodo y que recogiera los datos dados por la base de datos)</li>
                                                    <li>Los scripts (formados con la url dada por laravel)</li>
                                                </ol>
                                                <br>
                                                Entonces lo único que tuve que hacer es crear un layout general que incluyera el contenido de la carpeta PARTIALS y demás componentes de HTML. <br>
                                                Mediante blade creé el contenedor del "contenido principal" del layout usando "yield" y de este layout fuí extendiendo <br>
                                                A todas y cada una de las vistas contenidas dentro de RESOURCES > VIEWS > ADMIN.
                                                 </p>
                                            </li>
                                        </ul>

                                        <br><br>

                                        <h4>Resumen</h4>

                                        <p>
                                            Al final acabé con un app.blade.php que se estendía a todas las vistas contenidas en admin/
                                            Este app.blade incluía: <br><br> El partial css.blade.php (con todos los links a los .css de la plantilla)en primer lugar para darle estilo a todo el contenido posterior.
                                            <br>Seguidamente se incluye un poco de código html necesario para formar el desplegable que muestre los datos del usuario<br><br>
                                            A continuación se incluye el partial menu.blade.php, para hacer que los CRUD de Menús mostraran el contenido aqui
                                            <br><br>
                                            Después, creo un contenedor usando blade para la información del contenido (migas de pan, título y subtítulo de la vista que se está visualizando).
                                            <br>
                                            <br>
                                            Finalmente, se crea otro contenedor para el propio contenido y tras esto incluye el partial scripts.blade.php (con todos los scripts src="URL" a los .js de la plantilla)
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Tab Trending -->
                </div>
                <!-- /Tab Panes -->

            </div>
            <!-- /Product Tabs -->

        </div>
        <!-- /Container -->

    </div>



@endsection


@section('content-scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            var $slider =$('.bxslider1');


            var slider = $slider.bxSlider({
                adaptiveHeight: true,
                mode: 'horizontal',
                nextText: '',
                prevText: '',
                auto: true,
                pause: 5000
            });

        });


    </script>

@endsection