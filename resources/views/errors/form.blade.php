@if(! $errors->isEmpty())


    <div class="row">

        <div class="alert alert-danger alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul class="">
            @foreach($errors->all() as $error)
                <li class="">{{$error}}</li>
            @endforeach
            </ul>
        </div>

    </div>


@endif
