@extends('layouts.admin.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    <a class="btn btn-info" href="{{url()->previous()}}">{{trans("admin.actions.goback")}}</a>
                </p>
                @include('errors.form')


                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.categorys.create.title")}}</div>
                        <div class="panel-body">

                            {{ Form::open(['route' => 'category.store', 'method' => 'post' , 'class' => 'form']) }}

                            @include('admin.category.partials.fields')

                            <div class="form-group pull-right">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-success']) }}
                                <a href="{{route('category.index')}}" class="btn btn-primary">{{trans("admin.actions.cancel")}}</a>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>


                </ul>
            </div>
        </div>
    </div>

@endsection

