@extends('layouts.admin.app')
@section('content-info')
    <section class="content-header">
        <h1>
            {{trans("admin.category.title")}}
            <small>{{trans("admin.category.subtitle")}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>{{trans("admin.category.title")}}</a></li>
            <li class="active">{{trans("admin.category.here")}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('admin.partials.alert')

                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.category.table.title")}}</div>
                        <div class="panel-body">
                            <p>

                                <a class="btn btn-info" href="{{ route('category.create')}}">{{trans("admin.category.table.add")}}</a>

                            </p>


                            @if($category->count() < 1)
                                <h3 align="center"><strong>{{trans("admin.error.ups")}}</strong></h3>
                                <h4 align="center"> {{trans("admin.error.description1")}}</h4>
                                <p align="center">
                                    {{trans("admin.error.description2")}}
                                    <br><br>
                                </p>
                            @else
                                <!-- Table -->
                                @include('admin.category.partials.table')
                                {{--{!! $users->render() !!}--}}
                                <!-- /Table -->
                            @endif

                        </div>


                    </div>


                </ul>
            </div>
        </div>
    </div>
@endsection