<div class="form-group">
    {{ Form::label('name',trans("admin.category.table.name")) }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder' => trans("admin.category.create.name")]) }}
</div>

<div class="form-group">
    {{ Form::label('description',trans("admin.category.table.description")) }}
    {{ Form::textarea('description',null,['class'=>'form-control','placeholder' => trans("admin.category.create.description")]) }}
</div>


