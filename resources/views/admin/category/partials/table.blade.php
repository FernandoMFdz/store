<table class="table">
    <tr>
        <th>#</th>
        <th>{{trans("admin.category.table.name")}}</th>
        <th>{{trans("admin.category.table.description")}}</th>
        <th>{{trans("admin.category.table.slug")}}</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($categories as $category)
        <tr>
            <td>
                {{$category->id}}
            </td>
            <td>
                {{$category->name}}
            </td>

            <td>
                {{$category->description}}
            </td>
            <td>
                {{url('/'.$category->slug)}}
            </td>
            <td>
                <a class="btn btn-warning" href="{{route('category.edit',$category->id)}}">{{trans("admin.actions.edit")}}</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['category.destroy', $category->id]]) !!}
                {!!  Form::submit(trans("admin.actions.delete"),['class'=>'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
</table>