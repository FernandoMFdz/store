@extends('layouts.admin.app')

@section('content')
            <div class="col-md-10 col-md-offset-1">

                @include('errors.form')

                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.users.edit_title")}}: {{$user->name}} </div>
                        <div class="panel-body">
                            {{Form::model($user, ['route' => ['users.update', $user->id],'method'=>'PUT'])}}

                            @include('admin.users.partials.fields')


                            <div class="form-group">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-warning']) }}
                            </div>



                            {{ Form::close() }}
                            @include('admin.users.partials.delete')

                        </div>
                    </div>


                </ul>
            </div>

@endsection

