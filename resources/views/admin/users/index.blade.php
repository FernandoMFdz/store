@extends('layouts.admin.app')
@section('content-info')
    <section class="content-header">
        <h1>
            {{trans("admin.users.title")}}
            <small>{{trans("admin.users.subtitle")}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>{{trans("admin.users.title")}}</a></li>
            <li class="active">{{trans("admin.users.here")}}</li>
        </ol>
    </section>
@endsection
@section('content')

            <div class="col-md-10 col-md-offset-1">
                @include('admin.partials.alert')

                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.users.table.title")}} </div>
                        <div class="panel-body">
                            <div class="row">
                            {!! Form::open(['route'=> 'users.index','method' => 'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
                                <div class="form-group">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans("admin.users.search")]) !!}
                                    {{ Form::select('rol',config('options.rol'.trans("admin.users.lang")),null,['class' => 'form-control']) }}
                                </div>

                            {!!  Form::submit(trans("admin.actions.search"),['class'=>'btn btn-default']) !!}

                            {!! Form::close() !!}
                                <div class="col-md-6">
                                    <p>
                                        <a class="btn btn-info pull-left" href="{{ route('users.create') }}">{{trans("admin.users.create.title")}}</a>
                                    </p>
                                </div>
                            </div>
                            @if($users->count() < 1)
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-3">
                                        <h3 align="center"><strong>{{trans("admin.empty.ups")}}</strong></h3>
                                        <h4 align="center"> {{trans("admin.empty.description1")}}</h4>
                                        <p align="center">
                                            {{trans("admin.empty.description2")}}
                                            <br><br>
                                        </p>
                                    </div>
                                </div>
                            @else
                            <!-- Table -->
                            @include('admin.users.partials.table')
                            {{ $users->appends($request->only(['name','rol']))->render() }}
                            @endif
                        </div>


                    </div>


                </ul>
            </div>
    {!! Form::open(['route' => ['users.destroy',':USER_ID'],'method' => 'DELETE','id' => 'form-delete']) !!}
    {!! Form::close() !!}

@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        $('.btn-delete').click(function(e){
            e.preventDefault();
            var row = $(this).parents('tr');
            var id = row.data('id');
            var form = $('#form-delete');
            var url = form.attr('action').replace(':USER_ID',id);
            var data = form.serialize();


            row.fadeOut();
            $.post(url,data,function (result)   {
                alert(result.message);
               /* $(row).remove();*/
            }).fail(function(){
                alert('El usuario no pudo ser eliminado')
                row.show();
            });
        });

    });
</script>

@endsection