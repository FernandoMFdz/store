<table class="table">
    <tr>
        <th>#</th>
        <th>{{trans("admin.users.table.name")}}</th>
        <th>{{trans("admin.users.table.email")}}</th>
        <th>{{trans("admin.users.table.rol")}}</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($users as $user)
        <tr data-id="{{$user->id}}">
            <td>
                {{$user->id}}
            </td>
            <td>
                {{$user->name}}
            </td>
            <td>
                {{$user->email}}
            </td>

            <td>{{strtoupper(str_replace('_', ' ', $user->rol)) }}</td>

            <td>
                <a href="{{route('users.edit',$user->id)}}">{{trans("admin.actions.edit")}}</a>
            </td>
            <td>
                @include('admin.users.partials.delete')
            </td>
        </tr>
    @endforeach
</table>