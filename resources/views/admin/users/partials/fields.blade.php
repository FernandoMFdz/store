
<div class="form-group">
    {{ Form::label('name',trans("admin.users.table.name")) }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder' => trans("admin.users.create.name")]) }}
</div>

<div class="form-group">
    {{ Form::label('email',trans("admin.users.table.email")) }}
    {{ Form::email('email',null,['class'=>'form-control','placeholder' => trans("admin.users.create.email")]) }}
</div>

<div class="form-group">
    {{ Form::label('password',trans("admin.users.table.password")) }}
    {{ Form::password('password',['class'=>'form-control','placeholder' =>  trans("admin.users.create.password")]) }}
</div>

<div class="form-group">
    {{ Form::label('rol',trans("admin.users.table.rol")) }}
    {{ Form::select('rol',config('options.rol'.trans("admin.users.lang")),null,['class' => 'form-control']) }}
</div>