{!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id]]) !!}
{!! Form::submit(trans("admin.actions.delete"),['class'=>'btn btn-delete']) !!}
{!! Form::close() !!}