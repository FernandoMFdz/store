@extends('layouts.admin.app')

@section('content')
            <div class="col-md-10 col-md-offset-1">

                @include('errors.form')

                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.users.create.title")}}</div>
                        <div class="panel-body">
                            {{ Form::open(['route' => 'users.store', 'method' => 'post' , 'class' => 'form']) }}

                            @include('admin.users.partials.fields')

                            <div class="form-group">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-success']) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>


                </ul>
            </div>

@endsection

