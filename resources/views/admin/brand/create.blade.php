@extends('layouts.admin.app')

@section('content')

            <div class="col-md-12">
                <p>
                    <a class="btn btn-info" href="{{url('admin/brand')}}">{{trans("admin.actions.goback")}}</a>
                </p>
                @include('errors.form')


                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.brands.create.title")}}</div>
                        <div class="panel-body">

                            {{ Form::open(['route' => 'brand.store', 'method' => 'post' , 'class' => 'form']) }}

                            @include('admin.brand.partials.fields')

                            <div class="form-group pull-right">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-success']) }}
                                <a href="{{url('admin.brand')}}" class="btn btn-primary">{{trans("admin.actions.cancel")}}</a>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>


                </ul>
            </div>
@endsection

