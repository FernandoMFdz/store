@extends('layouts.admin.app')

@section('content')

            <div class="col-md-10 col-md-offset-1">
                @include('errors.form')


                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.lines.table.add")}}</div>
                        <div class="panel-body">

                            {{ Form::open(['route' => 'line.store', 'method' => 'post' , 'class' => 'form']) }}

                            @include('admin.brand.line.partials.fields')

                            <div class="form-group pull-right">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-success']) }}
                                <a href="{{route('line.index')}}" class="btn btn-primary">{{trans("admin.actions.cancel")}}</a>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>


                </ul>
            </div>

@endsection

