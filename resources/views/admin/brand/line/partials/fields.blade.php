<div class="form-group">
    {{ Form::label('name',trans("admin.lines.table.name")) }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder' => trans("admin.lines.create.name")]) }}
</div>

<div class="form-group">
    {{ Form::label('description',trans("admin.lines.table.description")) }}
    {{ Form::textarea('description',null,['class'=>'form-control','placeholder' => trans("admin.lines.create.description")]) }}
</div>

@if(empty($brand->id))
<div class="form-group">
    {{ Form::label('brand_id',trans("admin.lines.table.brand")) }}
    {{ Form::select('brand_id', $array,null,['placeholder'=>trans("admin.lines.create.brand"),'class'=>'form-control']) }}
</div>
@endif
