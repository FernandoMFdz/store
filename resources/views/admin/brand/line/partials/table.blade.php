<table class="table">
    <tr>
        <th>#</th>
        <th>{{trans("admin.lines.table.name")}}</th>
        <th>{{trans("admin.lines.table.description")}}</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($lines as $line)
        <tr>
            <td>
                {{$line->id}}
            </td>
            <td>
                {{$line->name}}
            </td>

            <td>
                {{$line->description}}
            </td>
            <td>
                <a class="btn btn-warning" href="{{route('line.edit',$line->id)}}">{{trans("admin.actions.edit")}}</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['line.destroy', $line->id]]) !!}
                {!!  Form::submit(trans("admin.actions.delete"),['class'=>'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
</table>