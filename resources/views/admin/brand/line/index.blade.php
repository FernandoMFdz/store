@extends('layouts.admin.app')
@section('content-styles')
    <style>
        .pagination{
            margin:-6px 0;
        }
    </style>
@endsection
@section('content')
    <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('admin.partials.alert')

                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.lines.table.title")}} </div>
                        <div class="panel-body">
                            <div class="row">
                                {!! Form::open(['route'=> 'line.index','method' => 'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
                                <div class="form-group">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans("admin.lines.search")]) !!}
                                </div>

                                {!!  Form::submit(trans("admin.actions.search"),['class'=>'btn btn-default']) !!}

                                {!! Form::close() !!}
                                <div class="col-md-6">
                                    <p>
                                        <a class="btn btn-info pull-left" href="{{ route('line.create')}}">{{trans("admin.lines.table.add")}}</a>
                                    </p>
                                </div>
                            </div>

                            @if($lines->count() < 1)
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-3">
                                        <h3 align="center"><strong>{{trans("admin.empty.ups")}}</strong></h3>
                                        <h4 align="center"> {{trans("admin.empty.description1")}}</h4>
                                        <p align="center">
                                            {{trans("admin.empty.description2")}}
                                            <br><br>
                                        </p>
                                    </div>
                                </div>
                            @else
                                <!-- Table -->
                                @include('admin.brand.line.partials.table')

                                <!-- /Table -->
                            @endif
                        </div>


                    </div>



                </ul>
                {{ $lines->appends($request->only(['name']))->render() }}
            </div>
    </div>
@endsection