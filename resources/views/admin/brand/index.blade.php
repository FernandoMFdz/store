@extends('layouts.admin.app')
@section('content-info')
    <section class="content-header">
        <h1>
            {{trans("admin.brands.title")}}
            <small>{{trans("admin.brands.subtitle")}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>{{trans("admin.brands.title")}}</a></li>
            <li class="active">{{trans("admin.brands.here")}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.brands.table.title")}}</div>
                        <div class="panel-body">
                            <div class="row">
                                {!! Form::open(['route'=> 'brand.index','method' => 'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
                                <div class="form-group">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans("admin.brands.search")]) !!}
                                </div>

                                {!!  Form::submit(trans("admin.actions.search"),['class'=>'btn btn-default']) !!}

                                {!! Form::close() !!}
                                <div class="col-md-6">
                                    <p>
                                        <a class="btn btn-info pull-left" href="{{ route('brand.create')}}">{{trans("admin.brands.create.title")}}</a>
                                    </p>
                                </div>
                            </div>

                            @if($brands->count() < 1)
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-3">
                                        <h3 align="center"><strong>{{trans("admin.empty.ups")}}</strong></h3>
                                        <h4 align="center"> {{trans("admin.empty.description1")}}</h4>
                                        <p align="center">
                                            {{trans("admin.empty.description2")}}
                                            <br><br>
                                        </p>
                                    </div>
                                </div>
                            @else
                            <!-- Table -->
                            @include('admin.brand.partials.table')
                            <!-- /Table -->
                            {{ $brands->appends($request->only(['name']))->render() }}
                            @endif
                        </div>
                    </div>
                </ul>
            </div>
    </div>
@endsection