<table class="table">
    <tr>
        <th>#</th>
        <th>{{trans("admin.brands.table.name")}}</th>
        <th>{{trans("admin.brands.table.email")}}</th>
        <th>{{trans("admin.brands.table.phone")}}</th>
        <th>{{trans("admin.brands.table.address")}}</th>
        <th>{{trans("admin.brands.table.nif_cif")}}</th>
        <th>{{trans("admin.brands.table.lines")}}</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($brands as $brand)
        <tr>
            <td>
                <a href="{{route('brand.show',$brand->id)}}">{{$brand->id}}</a>
            </td>
            <td>
                <a href="{{route('brand.show',$brand->id)}}">{{$brand->name}}</a>
            </td>
            <td>
                {{$brand->email}}
            </td>
            <td>
                {{$brand->phone}}
            </td>
            <td>
                {{$brand->address}}
            </td>
            <td>
                {{$brand->nif_cif}}
            </td>

            <td>
                @if($brand->lines->count() > 0)
                    <span class="label label-success">{{$brand->lines->count()}}</span>
                @else
                    <span class="label label-warning">{{$brand->lines->count()}}</span>
                @endif
            </td>
            <td>
                <a class="btn btn-warning" href="{{route('brand.edit',$brand->id)}}">{{trans("admin.actions.edit")}}</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['brand.destroy', $brand->id]]) !!}
                {!!  Form::submit(trans("admin.actions.delete"),['class'=>'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
</table>