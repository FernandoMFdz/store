<div class="form-group">
    {{ Form::label('name',trans("admin.brands.table.name")) }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.name")]) }}
</div>

<div class="form-group">
    {{ Form::label('email',trans("admin.brands.table.email")) }}
    {{ Form::email('email',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.email")]) }}
</div>

<div class="form-group">
    {{ Form::label('phone',trans("admin.brands.table.phone")) }}
    {{ Form::text('phone',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.phone")]) }}
</div>

<div class="form-group">
    {{ Form::label('address',trans("admin.brands.table.address")) }}
    {{ Form::text('address',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.address")]) }}
</div>

<div class="form-group">
    {{ Form::label('nif_cif',trans("admin.brands.table.nif_cif")) }}
    {{ Form::text('nif_cif',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.nif_cif")]) }}
</div>

<div class="form-group">
    {{ Form::label('web',trans("admin.brands.table.web")) }}
    {{ Form::text('web',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.web")]) }}
</div>

<div class="form-group">
    {{ Form::label('twitter',trans("admin.brands.table.twitter")) }}
    {{ Form::text('twitter',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.twitter")]) }}
</div>

<div class="form-group">
    {{ Form::label('facebook',trans("admin.brands.table.facebook")) }}
    {{ Form::text('facebook',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.facebook")]) }}
</div>

<div class="form-group">
    {{ Form::label('contact_name',trans("admin.brands.table.contact_name")) }}
    {{ Form::text('contact_name',null,['class'=>'form-control','placeholder' => trans("admin.brands.create.contact_name")]) }}
</div>