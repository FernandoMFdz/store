@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    <a class="btn btn-info" href="{{route('brand.index')}}">{{trans("admin.actions.goback")}}</a>
                </p>
                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            <div class="pull-left">
                                <span align="left">{{$brand->name}}</span>
                            </div>
                            <p align="right">

                                <a class="btn btn-warning" href="{{route('brand.edit',$brand->id)}}">{{trans("admin.actions.edit")}}</a>
                                <a class="btn btn-info" href="{{ url('admin/line/create/'.$brand->id)}}">{{trans("admin.brands.add_line")}}</a>
                            </p>

                        </div>
                        <div class="panel-body">

                            <p>
                                {{trans("admin.brands.table.email")}}:  {{$brand->email}}
                            </p>

                            <p>
                                {{trans("admin.brands.table.phone")}}: {{$brand->phone}}
                            </p>

                            <p>
                                {{trans("admin.brands.table.address")}}: {{$brand->address}}
                            </p>

                            <p>
                                {{trans("admin.brands.table.nif_cif")}}: {{$brand->nif_cif}}
                            </p>
                            <p>
                                {{trans("admin.brands.table.web")}}: {{$brand->web}}
                            </p>
                            <p>
                                {{trans("admin.brands.table.twitter")}}: {{$brand->twitter}}
                            </p>
                            <p>
                                {{trans("admin.brands.table.facebook")}}: {{$brand->facebook}}
                            </p>
                            <p>
                                {{trans("admin.brands.table.contact_name")}}: {{$brand->contact_name}}
                            </p>

                        </div>
                    </div>
                    <br>
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.lines.title")}} </div>
                        <div class="panel-body">
                            @if(count($brand->lines) > 0)
                                @foreach($brand->lines as $line)
                                    <div class="panel panel-default">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">{{$line->name}} </div>
                                        <div class="panel-body">
                                            <p>
                                                <a class="btn btn-warning" href="{{ route('line.edit',$line->id)}}">{{trans("admin.actions.edit")}}</a>
                                            </p>
                                            <p>
                                                {{trans("admin.lines.table.name")}}:  {{$line->name}}
                                            </p>

                                            <p>
                                                {{trans("admin.lines.table.description")}}: {{$line->description}}
                                            </p>


                                        </div>
                                    </div>

                                @endforeach
                                @else

                            @endif
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    </div>
@endsection