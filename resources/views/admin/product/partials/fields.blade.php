<div class="form-group">
    {{ Form::label('name',trans("admin.products.table.name")) }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder' => trans("admin.products.create.name")]) }}
</div>

<div class="form-group">
    {{ Form::label('sell_price',trans("admin.products.table.sell_price")) }}
    {{ Form::text('sell_price',null,['class'=>'form-control','placeholder' => trans("admin.products.create.sell_price")]) }}
</div>

<div class="form-group">
    {{ Form::label('discounted_price',trans("admin.products.table.discounted_price")) }}
    {{ Form::text('discounted_price',null,['class'=>'form-control','placeholder' => trans("admin.products.create.discounted_price")]) }}
</div>

<div class="form-group">
    {{ Form::label('cost_price',trans("admin.products.table.cost_price")) }}
    {{ Form::text('cost_price',null,['class'=>'form-control','placeholder' => trans("admin.products.create.cost_price")]) }}
</div>

<div class="form-group">
    {{ Form::label('description',trans("admin.products.table.description")) }}
    {{ Form::textarea('description',null,['class'=>'form-control','placeholder' => trans("admin.products.create.description")]) }}
</div>

<div class="form-group">
    {{ Form::label('line_id','Line') }}
    {{ Form::select('line_id', $line,null,['placeholder' =>trans("admin.products.table.pick_line"),'class'=>'form-control js-example-responsive']) }}
</div>

<div class="form-group">
    {{ Form::label('category_id','Categoria') }}
    {{ Form::select('category_id', $category,null,['placeholder' =>trans("admin.products.table.pick_category"),'class'=>'form-control js-example-responsive']) }}
</div>

