<table class="table">
    <tr>
        <th>#</th>
        <th>{{trans("admin.products.table.name")}}</th>
        <th>{{trans("admin.products.table.description")}}</th>
        <th>{{trans("admin.products.table.sell_price")}}</th>
        <th>{{trans("admin.products.table.cost_price")}}</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($products as $product)
        @if($product->category == null)
            <tr class="danger"data-toggle="tooltip" title="Some category was vinculated to this product, please fix that problem or it won't appear in the store">
        @elseif($product->line == null)
            <tr class="danger"data-toggle="tooltip" title="Some line was vinculated to this product, please fix that problem or it won't appear in the store">
        @elseif($product->category == null && $product->line == null)
            <tr class="danger"data-toggle="tooltip" title="Some category and any line were vinculated to this product, please fix that problem or it won't appear in the store">
        @else
            <tr>
        @endif
            <td>
                {{$product->id}}
            </td>
            <td>
                {{$product->name}}
            </td>

            <td>
                {{$product->details->description}}
            </td>
            <td>
                {{$product->sell_price}}
            </td>
            <td>
                {{$product->details->cost_price}}
            </td>
            <td>
                <a class="btn btn-warning" href="{{route('product.edit',$product->id)}}">{{trans("admin.actions.edit")}}</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['product.destroy', $product->id]]) !!}
                {!!  Form::submit(trans("admin.actions.delete"),['class'=>'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
</table>