@extends('layouts.admin.app')
@section('content-info')
    <section class="content-header">
        <h1>
            {{trans("admin.products.title")}}
            <small>{{trans("admin.products.subtitle")}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>{{trans("admin.products.title")}}</a></li>
            <li class="active">{{trans("admin.products.here")}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.category.table.title")}}</div>
                        <div class="panel-body">
                            <div class="row">
                                {!! Form::open(['route'=> 'product.index','method' => 'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
                                <div class="form-group">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans("admin.products.search")]) !!}
                                </div>

                                {!!  Form::submit(trans("admin.actions.search"),['class'=>'btn btn-default']) !!}

                                {!! Form::close() !!}
                                <div class="col-md-6">
                                    <p>
                                        <a class="btn btn-info pull-left" href="{{ route('product.create')}}">{{trans("admin.products.create.title")}}</a>
                                    </p>
                                </div>
                            </div>

                            @if($products->count() < 1)
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-3">
                                        <h3 align="center"><strong>{{trans("admin.empty.ups")}}</strong></h3>
                                        <h4 align="center"> {{trans("admin.empty.description1")}}</h4>
                                        <p align="center">
                                            {{trans("admin.empty.description2")}}
                                            <br><br>
                                        </p>
                                    </div>
                                </div>
                            @else
                                <!-- Table -->
                                @include('admin.product.partials.table')

                                {{ $products->appends($request->only(['name']))->render() }}
                                <!-- /Table -->
                            @endif

                        </div>


                    </div>


                </ul>
            </div>
    </div>
@endsection

@section('content-scripts')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection