@extends('layouts.admin.app')

@section('content-styles')

    <link rel="stylesheet" href="{{url('/')}}/cp/plugins/select2/select2.min.css">
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    <a class="btn btn-info" href="{{url()->previous()}}">{{trans("admin.actions.goback")}}</a>
                </p>
                @include('errors.form')


                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.products.create.title")}}</div>
                        <div class="panel-body">

                            {{ Form::open(['route' => 'product.store', 'method' => 'post' , 'class' => 'form']) }}

                            @include('admin.product.partials.fields')

                            <div class="form-group pull-right">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-success']) }}
                                <a href="{{route('product.index')}}" class="btn btn-primary">{{trans("admin.actions.cancel")}}</a>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>


                </ul>
            </div>
        </div>
    </div>

@endsection


@section('content-scripts')

    <script src="{{url('/')}}/cp/plugins/select2/select2.full.min.js"></script>

    <script>

        $(document).ready(function() {
            $(".js-example-responsive").select2();
        });
    </script>

@endsection

