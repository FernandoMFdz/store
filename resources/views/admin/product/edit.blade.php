@extends('layouts.admin.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    <a class="btn btn-info" href="{{route('product.index')}}">{{trans("admin.actions.goback")}}</a>
                </p>
                @include('errors.form')


                <ul class="list-group">

                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">{{trans("admin.products.edit_title")}} {{$product->name}} </div>
                        <div class="panel-body">
                            {{Form::model($product, ['route' => ['product.update', $product->id],'method'=>'PUT'])}}

                            @include('admin.product.partials.fields')

                            <div class="form-group pull-right">
                                {{ Form::submit(trans("admin.actions.save"),['class'=>'btn btn-success']) }}
                                <a href="{{route('product.index')}}" class="btn btn-primary">{{trans("admin.actions.cancel")}}</a>
                            </div>



                            {{ Form::close() }}
                        </div>
                    </div>


                </ul>
            </div>
        </div>
    </div>

@endsection

