                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>{{trans("admin.orders.table.reference")}}</th>
                            <th>{{trans("admin.orders.table.description")}}</th>
                            <th>{{trans("admin.orders.table.quantity")}}</th>
                            <th>{{trans("admin.orders.table.unit_price")}}</th>
                            <th>{{trans("admin.orders.table.total_price")}}</th>
                        </tr>
                    </thead>

                    <tbody>
                @foreach($order->details as $detail)
                    <tr>
                        <td>{{$detail->product_id}}</td>
                        <td>{{$detail->product->name}}</td>
                        <td>{{$detail->quantity}}</td>
                        <td>{{$detail->product->sell_price}}€</td>
                        <td>{{$detail->quantity*$detail->product->sell_price}}€</td>
                    </tr>

                @endforeach

                    </tbody>
                </table>



