<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <link rel="stylesheet" href="{{url('/')}}/cp/bootstrap/css/bootstrap.min.css">

</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-6 pull-left">
            <h1>
                <a href="{{url('/')}}">
                    <img src="{{url('/')}}/img/logolisto.png" style="width: 250px; height: auto">
                </a>
            </h1>
        </div>
        <div class="col-xs-6 pull-right">
            <h1>Factura</h1>
            <h1><small>Order #0000{{$order->id}}</small></h1>
        </div>
    </div>
    @include('admin.order.partials.list')
    <div class="row ">
        <div class="col-xs-6">
            <p>
                <strong>
                    Sub Total : <br>
                    {{trans("admin.orders.table.tax")}} : <br>
                    Total : <br>
                </strong>
            </p>
        </div>
        <div class="col-xs-6">
            <strong>
                {{$order->total}}€ <br>
                N/A <br>
                {{$order->total}}€ <br>
            </strong>
        </div>
    </div>
</div>
</body>
</html>
