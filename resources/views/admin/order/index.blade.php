@extends('layouts.admin.app')
@section('content-info')
    <section class="content-header">
        <h1>
            {{trans("admin.orders.title")}}
            <small>{{trans("admin.orders.subtitle")}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>{{trans("admin.orders.title")}}</a></li>
            <li class="active">{{$request->get('status')?$request->get('status'):trans("admin.orders.status.Paid")}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['route'=> 'order.index','method' => 'GET','class'=>'navbar-form ','role'=>'search']) !!}
                    <div class="form-group">
                        {!! Form::number('id',null,['class'=>'form-control','placeholder'=>trans("admin.orders.order_code")]) !!}
                    </div>
                    <div class="form-group">
                        {{ Form::select('status',config('options.order_status'.trans("admin.orders.lang")),null,['class' => 'form-control']) }}
                    </div>
                    {!!  Form::submit(trans("admin.actions.search"),['class'=>'btn btn-default']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {{--

                No meto esto en un include por que para generar la Factura necesito un listado distinto a este
                así que he adaptado el listado para que sea válido en ambos

            --}}
            @foreach($orders as $order)
                <div class="panel-group">
                    <div class="panel panel-default">
                        <a><div class="panel-heading" data-toggle="collapse" href="#collapse{{$order->id}}">
                                <h4 class="panel-title">
                                    {{trans("admin.orders.singular")}} No. {{ $order->id }} <span class="pull-right label label-warning">{{trans("admin.orders.status.".$order->status)}}</span>
                                </h4>
                            </div></a>
                        <div id="collapse{{$order->id}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                    @include('admin.order.partials.list')
                                <table class="table table-hover">
                                    <thead class="text-center">
                                    <tr>
                                        <th class="text-center" colspan="2">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="text-center">
                                        <td><h3>{{$order->total}}€</h3></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="panel-footer">
                                    <p>
                                        <a href="{{route('order.pdf',$order->id)}}" class="btn btn-success">{{trans("admin.orders.export_invoice")}}</a>
                                        <button class="btn btn-success">{{trans("admin.orders.mark_preparing")}}</button>
                                        <span class="label label-success pull-right">{{$order->updated_at}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{ $orders->appends($request->only(['status']))->render() }}
        </div>
    </div>
@endsection