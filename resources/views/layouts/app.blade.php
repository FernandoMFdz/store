<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Alumno20</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    @include('layouts.store.partials.css')

</head>

<body class="tile-x-bg no-follow">

<!-- /HEADER -->
<div class="page-wrapper">
    <!-- Header Block
    ============================================== -->
    <header class="header-block">
        <!--  Main Header -->
        <!-- Main Header
         ............................................ -->
        <div class="main-header container marca-store">

            <!-- Header Cols -->
            <div class="header-cols">

                <!-- Brand Col -->
                <div class="brand-col hidden-xs">

                    <!-- vcenter -->
                    <div class="vcenter">
                        <!-- v-centered -->
                        <div class="vcenter-this">

                        </div>
                        <!-- v-centered -->
                    </div>
                    <!-- vcenter -->

                </div>
                <!-- /Brand Col -->
                <!-- Right Col -->
                <div class="right-col">

                    <!-- vcenter -->
                    <div class="vcenter">

                        <!-- v-centered -->
                        <div class="vcenter-this">


                        </div>
                        <!-- /v-centered -->
                    </div>
                    <!-- /vcenter -->

                </div>
                <!-- /Right Col -->

                <!-- Left Col -->
                <div class="left-col">

                    <!-- vcenter -->
                    <div class="vcenter">

                        <!-- v-centered -->
                        <div class="vcenter-this">

                            <!--  <form class="header-search">
                               <div class="form-group">
                                 <input class="form-control" placeholder="SEARCH" type="text">
                                 <button class="btn btn-empty"><i class="fa fa-search"></i></button>
                               </div>
                             </form> -->

                        </div>
                        <!-- v-centered -->

                    </div>
                    <!-- vcenter -->

                </div>
                <!-- /Left Col -->
            </div>
            <!-- Header Cols -->

        </div>
        <!-- /Main Header
        .............................................. -->
        <!-- /Main Header-->

        <!-- Nav Bottom XS.............................................. -->

        <nav class="nav-bottom hnav hnav-ruled white-bg boxed-section">

            <!-- Container -->
            <div class="container">

                <!-- Header-->
                <div class="navbar-btn-group btn-group navbar-right pull-right" style="margin-top:8px; margin-bottom:-10px; ">
                    <button aria-expanded="false" class="btn btn-outline-menu visible-xs" style="margin-top:8px;"><i class="ti ti-pencil-alt"></i></button>
                    <button class=" btn btn-outline-menu visible-xs" style="margin-top:8px;"><i class="ti ti-user"></i></button>
                </div>
                <!-- /Header-->
                <!-- /Nav Bottom XS.............................................. -->


                <!-- Nav Bottom LG.............................................. -->
                <!-- Navbar btn-group -->
                <div class="navbar-btn-group btn-group navbar-right no-margin-r-xs hidden-xs">

                    <!-- Nav Side -->
                    <nav class="bottom hnav hnav-ruled white-bg boxed-section" role="navigation">
                        <!-- Dont Collapse -->
                        <div class="navbar-dont-collapse no-toggle">


                            <!-- Nav Right -->
                            <ul class="nav navbar-nav navbar-right case-u active-bcolor navbar-center-xs">

                                @if (Auth::guest())
                                <li class="dropdown has-panel">
                                    <a aria-expanded="false" href="{{url('login')}}"><i
                                                class="icon-left ti ti-user hidden-lg"></i><i
                                                class="hidden-xs hidden-sm hidden-md" style="font-style: normal;">
                                            {{trans('store.layout.signin')}}
                                        </i></a>
                                    <!-- /Dropdown Panel -->
                                </li>

                                <li class="dropdown has-panel">

                                    <a aria-expanded="false" href="{{url('register')}}"><i
                                                class="fa fa-edit hidden-lg"></i><i
                                                class="hidden-xs hidden-sm hidden-md" style="font-style: normal;">
                                            {{trans('store.layout.signup')}}

                                        </i></a>

                                </li>
                                @else
                                    <div class="btn-wrapper dropdown">

                                        <a aria-expanded="false" class="btn btn-cesta" data-toggle="dropdown"><b class="count count-scolor count-round">{{get_cart_item_count()}}</b><i class="ti ti-bag"></i></a>

                                        <!-- Dropdown Panel -->
                                        <div class="dropdown-menu dropdown-panel dropdown-right" data-keep-open="true">
                                            <section>
                                                <!-- Mini Cart -->
                                                <ul class="mini-cart">
                                                    {!! get_shopping_cart() !!}
                                                </ul>
                                                <!-- /Mini Cart -->
                                            </section>

                                            <section>
                                                <div class="row grid-10">
                                                    <div class="col-md-6">
                                                        <a class="btn btn-base btn-block margin-y-5" href="{{route('cart.index')}}">{{trans("store.layout.viewcart")}}</a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a class="btn btn-primary btn-block margin-y-5" href="#">{{trans("store.layout.checkout")}}</a>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                        <!-- /Dropdown Panel -->

                                    </div>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            @if(config('roles')[auth()->user()->rol] >= 3)
                                                <li>
                                                    @if(config('roles')[auth()->user()->rol] >= 5 )
                                                        <a href="{{ route('admin.index') }}">{{trans("store.layout.adminpanel")}}</a>
                                                    @else
                                                        <a href="{{ route('order.index') }}">{{trans("store.layout.adminpanel")}}</a>
                                                    @endif

                                                </li>
                                            @endif
                                            <li>
                                                <a href="{{ url('/logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    {{trans("store.layout.logout")}}
                                                </a>

                                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                            <!-- /Nav Right-->

                        </div>
                        <!-- /Dont Collapse -->
                    </nav>
                    <!-- /Nav Side -->
                </div>

                <!-- /Nav Bottom LG.............................................. -->

                <!-- SEARCH-GENERAL.............................................. -->
                {{--<div class="collapse navbar-collapse navbar-absolute navbar-ex1-collapse">
                    <!-- Navbar Center -->
                    <div class="nav navbar-nav navbar-center line-top line-pcolor case-c">





                    </div>
                    <!-- /Navbar Center -->

                </div>
                <!-- /Collapse -->--}}

                <!-- /SEARCH-GENERAL.............................................. -->


                <!-- LOGO -->
                <div class="navbar-btn-group btn-group navbar-left no-margin-r-xs">

                    <a href="{{route('home')}}">
                        <img src="{{url('/')}}/img/logolisto.png" data-toggle="tooltip"
                             style="width:144px; height:auto;margin-top: -24px;" data-placement="bottom" title="Innova Store">
                    </a>
                    <div class="cd-dropdown-wrapper">
                        <a style="padding-top: 41px;
margin-top: 4px;" class="cd-dropdown-trigger" href="#"></a>
                    </div> <!-- .cd-dropdown-wrapper -->
                </div>
                <!-- /LOGO-->
                <nav class="cd-dropdown">
                    <h2></h2>
                    <a href="#" class="cd-close">x</a>
                    <ul class="cd-dropdown-content">
                        <li><a href="{{route('home')}}">{{trans('store.layout.homemenu')}}</a></li>
                        {!!  get_menu() !!}
                        <li class="has-children">
                            <a href="#"> Menu Test</a>

                            <ul class="cd-secondary-dropdown is-hidden">
                                <li class="go-back"><a href="#">Menu</a></li>
                                <li class="see-all"><a class="btn btn-white" href="#">All Clothing</a></li>
                                <li class="has-children">
                                    <a href="#">Accessories</a>

                                    <ul class="is-hidden">
                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                        <li class="see-all"><a href="#">All Accessories</a></li>
                                        <li class="has-children">
                                            <a href="#0">Beanies</a>

                                            <ul class="is-hidden">
                                                <li class="go-back"><a href="#0">Accessories</a></li>
                                                <li class="see-all"><a href="#">All Benies</a></li>
                                                <li><a href="#">Caps &amp; Hats</a></li>
                                                <li><a href="#">Gifts</a></li>
                                                <li><a href="#">Scarves &amp; Snoods</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-children">
                                            <a href="#0">Caps &amp; Hats</a>

                                            <ul class="is-hidden">
                                                <li class="go-back"><a href="#0">Accessories</a></li>
                                                <li class="see-all"><a href="#">All Caps &amp; Hats</a></li>
                                                <li><a href="#">Beanies</a></li>
                                                <li><a href="#">Caps</a></li>
                                                <li><a href="#">Hats</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Glasses</a></li>
                                        <li><a href="#">Gloves</a></li>
                                        <li><a href="#">Jewellery</a></li>
                                        <li><a href="#">Scarves</a></li>
                                    </ul>
                                </li>

                                <li class="has-children">
                                    <a href="#">Bottoms</a>

                                    <ul class="is-hidden">
                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                        <li class="see-all"><a href="#">All Bottoms</a></li>
                                        <li><a href="#">Casual Trousers</a></li>
                                        <li class="has-children">
                                            <a href="#0">Jeans</a>

                                            <ul class="is-hidden">
                                                <li class="go-back"><a href="#0">Bottoms</a></li>
                                                <li class="see-all"><a href="#">All Jeans</a></li>
                                                <li><a href="#">Ripped</a></li>
                                                <li><a href="#">Skinny</a></li>
                                                <li><a href="#">Slim</a></li>
                                                <li><a href="#">Straight</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#0">Leggings</a></li>
                                        <li><a href="#0">Shorts</a></li>
                                    </ul>
                                </li>

                                <li class="has-children">
                                    <a href="#">Jackets</a>

                                    <ul class="is-hidden">
                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                        <li class="see-all"><a href="#">All Jackets</a></li>
                                        <li><a href="#">Blazers</a></li>
                                        <li><a href="#">Bomber jackets</a></li>
                                        <li><a href="#">Denim Jackets</a></li>
                                        <li><a href="#">Duffle Coats</a></li>
                                        <li><a href="#">Leather Jackets</a></li>
                                        <li><a href="#">Parkas</a></li>
                                    </ul>
                                </li>

                                <li class="has-children">
                                    <a href="#">Tops</a>

                                    <ul class="is-hidden">
                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                        <li class="see-all"><a href="#">All Tops</a></li>
                                        <li><a href="#">Cardigans</a></li>
                                        <li><a href="#">Coats</a></li>
                                        <li><a href="#">Polo Shirts</a></li>
                                        <li><a href="#">Shirts</a></li>
                                        <li class="has-children">
                                            <a href="#0">T-Shirts</a>

                                            <ul class="is-hidden">
                                                <li class="go-back"><a href="#0">Tops</a></li>
                                                <li class="see-all"><a href="#">All T-shirts</a></li>
                                                <li><a href="#">Plain</a></li>
                                                <li><a href="#">Print</a></li>
                                                <li><a href="#">Striped</a></li>
                                                <li><a href="#">Long sleeved</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Vests</a></li>
                                    </ul>
                                </li>
                            </ul> <!-- .cd-secondary-dropdown -->
                        </li> <!-- .has-children -->

                    </ul> <!-- .cd-dropdown-content -->
                </nav> <!-- .cd-dropdown -->
                <!-- Menu -->
            </div>
        </nav><!-- /Menu -->
    </header>
    <!-- /Header Block
============================================== -->
</div>
<!-- /HEADER -->

<!-- CONTENIDO -->
@yield('content')

<!-- /CONTENIDO -->

<!-- FOOTER -->
<footer>
    <!-- Container -->
    <div class="container cont-top clearfix">

        <!-- Row -->
        <div class="row">


            <!-- Links -->
            <div class="col-md-9 links-col">

                <!-- Row -->
                <div class="row-fluid">

                    <!-- Col -->
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <h5>{{trans('store.layout.aboutus.title')}}</h5>
                        <p>{{trans('store.layout.aboutus.description')}}</p>
                        <!-- hlinks -->
                        <ul class="hlinks hlinks-icons color-icons-borders color-icons-bg color-icons-hovered">
                            <li><a href="#"><i class="icon fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="icon fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="icon fa fa-rss"></i></a></li>
                            <li><a href="#"><i class="icon fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="icon fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="icon fa fa-youtube"></i></a></li>
                        </ul>
                        <!-- /hlinks -->
                    </div>
                    <!-- /Col -->

                    <!-- Col -->
                    <div class="col-xs-6 col-sm-3 col-md-3">
                        <h5>{{trans('store.layout.member.title')}}</h5>
                        <ul class="vlinks">
                            <li><a href="#">{{trans('store.layout.member.account')}}</a></li>
                            <li><a href="#">{{trans('store.layout.member.wishlist')}}</a></li>
                            <li><a href="#">{{trans('store.layout.member.history')}}</a></li>
                            <li><a href="#">{{trans('store.layout.member.cart')}}</a></li>
                        </ul>
                    </div>
                    <!-- /Col -->

                    <!-- Col -->
                    <div class="col-xs-6 col-sm-3 col-md-3 newsletter">
                        <h5>{{trans('store.layout.newsletter.title')}}</h5>
                        <form>
                            <input class="form-control" placeholder="{{trans('store.layout.newsletter.email')}}" type="text">
                            <br>
                            <button class="btn btn-outline" type="button">{{trans('store.layout.newsletter.subscribe')}}</button>
                        </form>
                    </div>
                    <!-- /Col -->

                </div>
                <!-- /Row -->

            </div>
            <!-- /Links -->

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

    <!-- Bottom -->
    <div style="padding:20px;">
        <span class="copy-text">© 2017 Alumno20</span>
        <!-- hlinks -->
        <ul class="hlinks pull-right">
            <li><a href="#">{{trans('store.layout.aboutus.title')}}</a></li>
            <li><a href="#">{{trans('store.layout.signin')}}</a></li>
            <li><a href="#">{{trans('store.layout.signup')}}</a></li>
            <li><a href="#">{{trans('store.layout.help')}}</a></li>
        </ul>
        <br>
    </div>
    <!-- /Bottom -->

</footer>

<!-- /FOOTER -->
<!-- JS -->
<!-- please note: The JavaScript files are loaded in the footer to speed up page construction -->
@include('layouts.store.partials.scripts')
</body>
</html>