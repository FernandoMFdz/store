<link rel="stylesheet" type="text/css"
      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
@foreach($links = config('scripts.store.css') as $link)
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/store/{{$link}}">
@endforeach
@yield('content-styles')

