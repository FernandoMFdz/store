<!-- REQUIRED JS SCRIPTS -->
<script type="text/javascript" src="{{url('/')}}/store/js/jquery-1.10.2.js"></script>
<script src="{{url('/')}}/store/js/bootstrap-toggle.js"></script>
@foreach($scripts = config('scripts.store.js') as $script)
    <script src="{{url('/')}}/store/uikit/{{$script}}"></script>
@endforeach
@yield('content-scripts')