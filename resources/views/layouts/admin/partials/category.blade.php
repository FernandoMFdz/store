<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="http://www.graciasalaansiedad.com/img/loginuser.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ ucfirst(Auth::user()->name) }}</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i>{{trans("admin.layout.status")}}</a>
        </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="{{trans("admin.layout.search")}}">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
    </form>
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li class="header">General</li>
        <!-- Optionally, you can add icons to the links -->
        @if(config('roles')[auth()->user()->rol] >= config('roles.director'))
        <li><a href="{{url('admin/')}}"><i class="fa fa-area-chart "></i> <span>{{trans("admin.dashboard.title")}}</span></a></li>
        @endif

        @if(config('roles')[auth()->user()->rol] >= config('roles.personal_tienda') && config('roles')[auth()->user()->rol] != config('roles.director'))
        <li><a href="{{url('admin/order')}}"><i class="fa fa-cart-arrow-down"></i> <span>{{trans("admin.orders.title")}}</span></a></li>
        @endif

        @if(config('roles')[auth()->user()->rol] >= config('roles.administrador') && config('roles')[auth()->user()->rol] != config('roles.director'))
        <li><a href="{{route('category.index')}}"><i class="fa fa-bars  "></i> <span>{{trans("admin.category.title")}}</span></a></li>
        @endif

        @if(config('roles')[auth()->user()->rol] >= config('roles.administrador') && config('roles')[auth()->user()->rol] != config('roles.director'))
        <li><a href="{{route('product.index')}}"><i class="fa fa-product-hunt "></i> <span>{{trans("admin.products.title")}}</span></a></li>
        @endif

        @if(config('roles')[auth()->user()->rol] >= config('roles.administrador') && config('roles')[auth()->user()->rol] != config('roles.director'))
        <li class="treeview">
            <a href="{{route('brand.index')}}"><i class="fa fa-ticket"></i> <span>{{trans("admin.brands.title")}}</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('brand.index')}}">{{trans("admin.brands.title")}}</a></li>
                <li><a href="{{route('line.index')}}">{{trans("admin.lines.title")}}</a></li>
            </ul>
        </li>
        @endif

        @if(config('roles')[auth()->user()->rol] >= config('roles.administrador'))
        <li><a href="{{route('users.index')}}"><i class="fa fa-user "></i><span>{{trans("admin.users.title")}}</span></a></li>
        @endif

    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->