<?php

use App\Brand;
use App\Line;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_ES');

            $name = "Apple";
            $brand = new Brand(['name'=>$name]);
            $brand->save();
            $brand->details()->create(
                [
                    'email' => $faker->companyEmail,
                    'phone' => $faker->phoneNumber,
                    'address' => $faker->address,
                    'nif_cif'=>$faker->creditCardNumber,
                    'web' => "http://www.".$faker->domainName,
                    'twitter' =>  "http://twitter.com/".str_slug($name,'_'),
                    'facebook' => "http://facebook.com/".str_slug($name,'.'),
                    'contact_name' => $faker->firstName . " ".$faker->lastName
                ]
            );
            $brand->lines()->create(
                [
                    'name' => 'iPhone',
                    'description' => 'The smartest phones'
                ]
            );

        $name = "Microsoft";
        $brand = new Brand(['name'=>$name]);
        $brand->save();
        $brand->details()->create(
            [
                'email' => $faker->companyEmail,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'nif_cif'=>$faker->creditCardNumber,
                'web' => "http://www.".$faker->domainName,
                'twitter' =>  "http://twitter.com/".str_slug($name,'_'),
                'facebook' => "http://facebook.com/".str_slug($name,'.'),
                'contact_name' => $faker->firstName . " ".$faker->lastName
            ]
        );

        $brand->lines()->create(
            [
                'name' => 'Windows',
                'description' => 'Just smart, just operative systems'
            ]
        );

        $name = "Asus";
        $brand = new Brand(['name'=>$name]);
        $brand->save();
        $brand->details()->create(
            [
                'email' => $faker->companyEmail,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'nif_cif'=>$faker->creditCardNumber,
                'web' => "http://www.".$faker->domainName,
                'twitter' =>  "http://twitter.com/".str_slug($name,'_'),
                'facebook' => "http://facebook.com/".str_slug($name,'.'),
                'contact_name' => $faker->firstName . " ".$faker->lastName
            ]
        );
        $brand->lines()->create(
            [
                'name' => 'Asus Gaming',
                'description' => 'Reveal the power of the gaming'
            ]
        );

        $name = "Sony";
        $brand = new Brand(['name'=>$name]);
        $brand->save();
        $brand->details()->create(
            [
                'email' => $faker->companyEmail,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'nif_cif'=>$faker->creditCardNumber,
                'web' => "http://www.".$faker->domainName,
                'twitter' =>  "http://twitter.com/".str_slug($name,'_'),
                'facebook' => "http://facebook.com/".str_slug($name,'.'),
                'contact_name' => $faker->firstName . " ".$faker->lastName
            ]
        );

        $brand->lines()->create(
            [
                'name' => 'PlayStation',
                'description' => 'Ultimate Gaming Center'
            ]
        );

        $brand->lines()->create(
            [
                'name' => 'Xperia',
                'description' => 'Top Gama Phones'
            ]
        );


        $name = "Samsung";
        $brand = new Brand(['name'=>$name]);
        $brand->save();
        $brand->details()->create(
            [
                'email' => $faker->companyEmail,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'nif_cif'=>$faker->creditCardNumber,
                'web' => "http://www.".$faker->domainName,
                'twitter' =>  "http://twitter.com/".str_slug($name,'_'),
                'facebook' => "http://facebook.com/".str_slug($name,'.'),
                'contact_name' => $faker->firstName . " ".$faker->lastName
            ]
        );

        $brand->lines()->create(
            [
                'name' => 'Galaxy S',
                'description' => 'Top Gama Phones'
            ]
        );
    }
}
