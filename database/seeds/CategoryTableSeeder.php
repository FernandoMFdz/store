<?php

use App\Category;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(
            [
                'name'=>'Pc Gaming',
                'description' => "The best pc gaming products of the market",
                'slug' => 'pc-gaming'
            ]
        );

        Category::create(
            [
                'name'=>'Smart Phones',
                'description' => "The best phones of the market",
                'slug' => 'smart-phones'
            ]
        );

        Category::create(
            [
                'name'=>'Consoles',
                'description' => "Ultimate Consoles",
                'slug' => 'consoles'
            ]
        );
    }
}
