<?php

use Illuminate\Database\Seeder;
use App\Brand;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = Brand::all();
        $faker = config('faker');

        foreach ($brands as $brand){

            foreach ($brand->lines as $line){
                $data = $faker[$brand->name][$line->name];

                foreach ($data as $product){
                    $producto = $line->products()->create($product['general']);
                    $producto->details()->create($product['details']);
                }
            }
        }
    }
}
