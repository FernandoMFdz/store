<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //
         $user = new User([
            'name' => 'Carlos Abrisqueta',
            'email' => 'cmabris@gmail.com',
            'password' =>"123456",
        ]);
        $user->save();
        $user->access()->create(['rol' => 'programador']);

        $user = new User([
            'name' => 'Fernando Meseguer',
            'email' => 'programador@gmail.com',
            'password' =>"123456",
        ]);
        $user->save();
        $user->access()->create(['rol' => 'programador']);

        $user = new User([
                    'name' => 'Jose Administrador',
                    'email' => 'administrador@gmail.com',
                    'password' =>"123456",
                    ]);
        $user->save();
        $user->access()->create(['rol' => 'administrador']);


        $user = new User([
            'name' => 'Adrian Director',
            'email' => 'director@gmail.com',
            'password' =>"123456",
        ]);
        $user->save();
        $user->access()->create(['rol' => 'director']);

        $user = new User([
            'name' => 'Antonio PersonalTienda',
            'email' => 'personaltienda@gmail.com',
            'password' =>"123456",
        ]);
        $user->save();
        $user->access()->create(['rol' => 'personal_tienda']);

        $user = new User([
            'name' => 'Sonia PersonalSalon',
            'email' => 'personalsalon@gmail.com',
            'password' =>"123456",
        ]);
        $user->save();
        $user->access()->create(['rol' => 'personal_salon']);

        $user = new User([
            'name' => 'Pepe Cliente',
            'email' => 'cliente@gmail.com',
            'password' =>"123456",
        ]);
        $user->save();
        $user->access()->create(['rol' => 'cliente']);

        factory(App\User::class, 50)->create()->each(function ($u) {
            $u->access()->save(factory(App\UserAccess::class)->make());
        });
    }
}
