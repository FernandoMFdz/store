<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    $name = $faker->randomElement();
    return [
        'name' => $faker->text(25),
        'description' => $faker->text(255),
        'slug' => \Illuminate\Support\Str::slug($name),
    ];
});
