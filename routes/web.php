<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Store\HomeController@index')->name('home');

Route::group(['prefix'=>'admin','namespace' => 'Admin','middleware'=>'role:personal_tienda'],
    function(){
        Route::get('/', 'DashboardController@index')->name('admin.index');
        Route::resource('brand','BrandController');
        Route::get('order/pdf/{order}','OrderController@pdf')->name('order.pdf');
        Route::resource('order','OrderController');
        Route::resource('line','LineController');
        Route::resource('category','CategoryController');
        Route::resource('product','ProductController');
        Route::get('users/restore/{id}','UserController@restore')->name('user.restore');
        Route::resource('users','UserController');
        Route::get('line/create/{brand}', 'LineController@create');
    }
);

Auth::routes(["verify" => true]);

Route::group(['namespace' => 'Store','middleware' => 'auth'], function() {
    Route::delete('order/detail/delete/{order_detail}', 'CartController@delItem')->name('cart.detail.delete');
    Route::put('cart/pay/{order}', 'CartController@pay')->name('cart.pay');
    Route::get('cart', 'CartController@cart')->name('cart.index');
    Route::post('cart/{product}', 'CartController@store')->name('cart.store');
});

Route::get('/home', 'Store\HomeController@index')->name('home');

Route::get('p/{product}', 'Store\ProductController@show')->name('p');

Route::get('{category}', 'Store\CategoryController@show', ['parameters' => [
    '/' => 'category'
]])->name('category');