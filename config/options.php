<?php

return [

    'rol' => [
                '' => 'Select Role',
                'programador' =>'Programmer',
                'director' => 'Director',
                'administrador' => 'Admin',
                'personal_tienda' => 'Shop Staff',
                'personal_salon' => 'Salon Staff',
                'cliente' => 'Costumer'
              ],
    'rol_es' => [
        '' => 'Selecciona el Rol',
        'programador' =>'Programador',
        'director' => 'Director',
        'administrador' => 'Administrador',
        'personal_tienda' => 'Personal de la Tienda',
        'personal_salon' => 'Personal del Salon',
        'cliente' => 'Cliente'
    ],

    'order_status' => [
        '' => 'Select Status (Paid)',
        'Pending' =>'Pending',
        'Paid' => 'Paid',
        'Preparing' => 'Preparing',
        'Returned' => 'Returned',
        'Closed' => 'Closed',
        'Shipped' => 'Shipped'
    ],
    'order_status_es' => [
        '' => 'Selecciona el Estado (Pagado)',
        'Pending' =>'Pendiente',
        'Paid' => 'Pagado',
        'Preparing' => 'En Preparación',
        'Returned' => 'Devuelto',
        'Closed' => 'Cerrado',
        'Shipped' => 'Enviado'
    ]


];