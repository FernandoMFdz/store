<?php


return [
        'Apple' => [
            'iPhone'=> [
                    0 => [
                            'general' => [
                                'name' => "iPhone 6S",
                                'slug'=>"iphone-6s",
                                'sell_price' => 640,
                                'discounted_price' => 639,
                                'category_id' => 2
                            ],
                            "details" => [
                                'description' => "The next iPhone",
                                'cost_price' => 150
                            ]
                        ],
                    1 => [
                            'general' => [
                                'name' => "iPhone 5S",
                                'slug'=>"iphone-5s",
                                'sell_price' => 400,
                                'discounted_price' => 200,
                                'category_id' => 2
                            ],
                            "details" => [
                                'description' => "The previous iPhone",
                                'cost_price' => 50
                            ]
                        ]
                    ]

        ],
    'Microsoft' => [
        'Windows'=> [
            0 => [
                'general' => [
                    'name' => "Windows 10",
                    'slug'=>"windows-10",
                    'sell_price' => 640,
                    'discounted_price' => 639,
                    'category_id' => 1
                ],
                "details" => [
                    'description' => "The next Windows",
                    'cost_price' => 150
                ]
            ],
            1 => [
                'general' => [
                    'name' => "Windows 8.1",
                    'slug'=>"windows-8-1",
                    'sell_price' => 400,
                    'discounted_price' => 200,
                    'category_id' => 1
                ],
                "details" => [
                    'description' => "The previous Windows",
                    'cost_price' => 50
                ]
            ]
        ]

    ],
    'Asus' => [
        'Asus Gaming'=> [
            0 => [
                'general' => [
                    'name' => "Asus Gaming Laptop X16",
                    'slug'=>"asus-gaming-laptop-x16",
                    'sell_price' => 800,
                    'discounted_price' => 639,
                    'category_id' => 1
                ],
                "details" => [
                    'description' => "The next Laptop",
                    'cost_price' => 150
                ]
            ],
            1 => [
                'general' => [
                    'name' => "Asus Gaming Laptop x14",
                    'slug'=>"asus-gaming-laptop-x14",
                    'sell_price' => 400,
                    'discounted_price' => 200,
                    'category_id' => 1
                ],
                "details" => [
                    'description' => "The previous Asus Gaming Laptop",
                    'cost_price' => 50
                ]
            ]
        ]

    ],
    'Sony' => [
        'PlayStation'=> [
            0 => [
                'general' => [
                    'name' => "PS4 PRO",
                    'slug'=>"ps4-pro",
                    'sell_price' => 300,
                    'discounted_price' => 200,
                    'category_id' => 3
                ],
                "details" => [
                    'description' => "The next Sony Console",
                    'cost_price' => 150
                ]
            ],
            1 => [
                'general' => [
                    'name' => "PS4 Slim",
                    'slug'=>"ps4-slim",
                    'sell_price' => 100,
                    'discounted_price' => 50,
                    'category_id' => 3
                ],
                "details" => [
                    'description' => "The previous PS4",
                    'cost_price' => 50
                ]
            ],
            2 => [
                'general' => [
                    'name' => "PS3 500Gb",
                    'slug'=>"ps3-500-gb",
                    'sell_price' => 100,
                    'discounted_price' => 50,
                    'category_id' => 3
                ],
                "details" => [
                    'description' => "The previous PS Console",
                    'cost_price' => 50
                ]
            ]
        ],
        'Xperia' =>  [
            0 => [
                'general' => [
                    'name' => "Xperia X10",
                    'slug'=>"xperia-x10",
                    'sell_price' => 800,
                    'discounted_price' => 639,
                    'category_id' => 2
                ],
                "details" => [
                    'description' => "The next Xperia",
                    'cost_price' => 150
                ]
            ],
            1 => [
                'general' => [
                    'name' => "Xperia X9",
                    'slug'=>"",
                    'sell_price' => 400,
                    'discounted_price' => 200,
                    'category_id' => 2
                ],
                "details" => [
                    'description' => "The previous Asus Gaming Laptop",
                    'cost_price' => 50
                ]
            ]

        ]
    ],
    'Samsung' => [
        'Galaxy S'=> [
            0 => [
                'general' => [
                    'name' => "Samsung Galaxy Note S Explode 7",
                    'slug'=>"samsung-galaxy-note-s-explode-7",
                    'sell_price' => 800,
                    'discounted_price' => 639,
                    'category_id' => 2
                ],
                "details" => [
                    'description' => "The next Explosion",
                    'cost_price' => 150
                ]
            ],
            1 => [
                'general' => [
                    'name' => "Samsung Galaxy S6",
                    'slug'=>"samsung-galaxy-s6",
                    'sell_price' => 400,
                    'discounted_price' => 200,
                    'category_id' => 2
                ],
                "details" => [
                    'description' => "The previous Explosion",
                    'cost_price' => 50
                ]
            ]
        ]

    ]
];