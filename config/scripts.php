<?php


return [

  "admin" =>[

  ],

  "store" => [
      "js" => [
          "plugins/owl-carousel/owl.carousel.min.js",
          "js/innova.js",
          "js/list.js",
          "js/product-carrousel.js",
          "js/uikit-utils.js",
          "js/jquery.bxslider-rahisified.js",
          "js/jquery-ui.min.js",
          "js/highlight.pack.js",
          "bootstrap/js/bootstrap.min.js",
          "js/jquery-scrollto.js",
          "js/jquery.prettyPhoto.js",
          "js/amcharts/amcharts.js",
          "js/amcharts/serial.js",
          "js/amcharts/pie.js",
          "js/summernote.js",
          "js/summernote-es-ES.js",
          "js/jquery.bxslider.js",
          "js/progressbar.js",
          "js/wow.min.js",
          "js/theme.js",
          "plugins/mega-dropdown/js/jquery.menu-aim.js",
          "plugins/mega-dropdown/js/modernizr.js",
          "plugins/mega-dropdown/js/main.js"

      ],
      "css" => [
          "uikit/plugins/owl-carousel/assets/owl.carousel.css",
          "uikit/bootstrap/css/bootstrap.min.css",
          "uikit/css/uikit.css",
          "css/list.css",
          "css/summernote.css",
          "css/innova.css",
          "css/summernote.css",
          "css/jquery-ui.css",
          "css/fileinput.min.css",
          "uikit/plugins/mega-dropdown/css/style.css"
      ]

  ]

];