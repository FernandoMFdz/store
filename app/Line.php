<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    protected $fillable = [
        'name', 'description', 'brand_id',
    ];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function scopeName($query,$name)
    {
        if (trim($name) != '') {
            $query->where('name','LIKE',"%$name%");
        }
    }
}
