<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name','description','slug'
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public static function getAllByName($name)
    {
        $result = Category::select()->where('slug','=',$name)->get();
        if ($result->isEmpty()) return null;
        $category= Category::findOrFail($result[0]->id);
        return $category;
    }
}
