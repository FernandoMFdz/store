<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
     //
     protected $fillable = [
        'name'
    ];

    public function details()
    {
        return $this->hasOne('App\BrandDetail');
    }

    public function lines()
    {
        return $this->hasMany('App\Line');
    }

    public function products()
    {
        return $this->lines->products;
    }

    public function setNameAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['name'] = $value ;
        }
    }

    public function getEmailAttribute()
    {
        return $this->details->email;
    }

    public function getPhoneAttribute()
    {
        return $this->details->phone;
    }

    public function getAddressAttribute()
    {
        return $this->details->address;
    }

    public function getNifCifAttribute()
    {
        return $this->details->nif_cif;
    }

    public function getWebAttribute()
    {
        return $this->details->web;
    }

    public function getTwitterAttribute()
    {
        return $this->details->twitter;
    }

    public function getFacebookAttribute()
    {
        return $this->details->facebook;
    }

    public function getContactNameAttribute()
    {
        return $this->details->contact_name;
    }

    public function scopeName($query,$name)
    {
        if (trim($name) != '') {
            $query->where('name','LIKE',"%$name%");
        }
    }
}
