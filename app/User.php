<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function access()
    {
        return $this->hasOne('App\UserAccess');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function getRolAttribute()
    {
        return $this->access->rol;
    }

    public function scopeName($query,$name)
    {
        if (trim($name) != '') {
            $query->where('name','LIKE',"%$name%");
        }
    }

    public function scopeRol($query, $rol)
    {
        $roles = config('options.rol');
        if ($rol != '' && isset($roles[$rol])) {
            $query->where('rol', $rol);
        }
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }
}
