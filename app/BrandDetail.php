<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandDetail extends Model
{
    protected $fillable = [
        'email', 'phone', 'address','nif_cif','web','twitter','facebook','contact_name','brand_id',
    ];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
}
