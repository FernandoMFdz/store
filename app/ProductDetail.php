<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    //
    protected $fillable = [
        'description', 'cost_price','product_id'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
