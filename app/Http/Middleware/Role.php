<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $hierarchy = config('roles');
        $user = auth()->user();
        if ($user === null){
            return redirect()->route('login');
        }

        if($hierarchy[$user->rol] < $hierarchy[$role]){
            abort(404);
        }
        return $next($request);
    }
}
