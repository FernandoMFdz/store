<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('role:administrador');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::name($request->get('name'))->whereHas('access',
            function($rol) use ($request){
                $rol->rol($request->get('rol'));
            }
        )
            ->paginate(5);
        return view('admin.users.index', compact('users','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $usr = new User($request->except('rol'));
        $usr->save();
        $usr->access()->create($request->all());
        return redirect()->route('users.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        Session::flash('user_id',$id);
//        dd($user);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        //
        $usr = User::findOrFail($id);
        $usr->fill($request->except(['rol']));
        $usr->access()->update($request->only(['rol']));
        $usr->save();
        return redirect()->route('users.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      if ($id === 3){
          return redirect()->route('users.index');
      }else {
          $user = User::findOrFail($id);
          $user->destroy($user->id);
          $message = "El usuario " . $user->name . " ha sido eliminado " . " <a href='".route('user.restore',$user->id)."' class='btn btn-danger'>Undo</a>";

          if ($request->ajax()) {
              return response()->json([
                  'id' => $user->id,
                  'message' => $message
              ]);
          }
          Session::flash('message', $message);
          return redirect()->route('users.index');
      }
    }

    public function restore($id)
    {

        $user = User::withTrashed()->where('id', '=', $id)->first();
        $user->restore();

        return redirect()->route('users.index');
    }
}
