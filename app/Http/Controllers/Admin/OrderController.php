<?php

namespace App\Http\Controllers\Admin;


use App;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:personal_tienda');
        $this->middleware('abort_if_role:director');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ( ! $request->input('id') && ! $request->input('status'))
            $orders = Order::status('Paid')->paginate(10);
        else
            $orders = Order::code($request->input('id'))->status($request->input('status'))->paginate(10);

        return view('admin.order.index', compact('orders','request'));
    }

    public function pdf(Order $order)
    {
        $view = view('admin.order.invoice.pdf',compact('order'))->render();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');
    }
}
