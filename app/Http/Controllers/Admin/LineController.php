<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LineRequest;
use App\Http\Controllers\Controller;
use App\Line;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LineController extends Controller
{

    function __construct()
    {
        $this->middleware('role:administrador');
        $this->middleware('abort_if_role:director');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lines = Line::name($request->get('name'))->paginate(10);
        return view('admin.brand.line.index', compact('lines','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Brand $brand)
    {
        $array = [];
        $brands = Brand::all();
        foreach ($brands as $bran){
            $array[$bran->id] =$bran->name;
        }
        if (!empty($brand->id)){
            Session::flash('brand_id',$brand->id);
        }

        return view('admin.brand.line.create', compact('array', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LineRequest $request)
    {
        $line = new Line($request->all());
        if (Session::has('brand_id')) $line->brand_id = Session::get('brand_id');
        $line->save();
        return redirect()->route('line.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Line $line
     * @return \Illuminate\Http\Response
     */
    public function show(Line $line)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Line $line
     * @return \Illuminate\Http\Response
     */
    public function edit(Line $line)
    {
        $array = get_model_selectable(Brand::all());

        Session::flash('line_id',$line->id);
        return view('admin.brand.line.edit', compact('line', 'array'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Line $line
     * @return \Illuminate\Http\Response
     */
    public function update(LineRequest $request, Line $line)
    {
        $line->fill($request->all());
        $line->save();
        return redirect()->route('line.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Line $line
     * @return \Illuminate\Http\Response
     */
    public function destroy(Line $line)
    {
        $count = $line->products->count();
        if ($count != 0){
            Session::flash('message', "$count products are having problems because of the deletion of $line->name line, please, create a new Line to fix this problem");
        }

        $line->delete();
        return redirect()->route('line.index');
    }
}
