<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use App\Line;
use App\Category;
use App\Product;
use Illuminate\Support\Str as Str;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    function __construct()
    {
        $this->middleware('abort_if_role:director');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::name($request->get('name'))->paginate(15);
        return view('admin.product.index',compact('products','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Con helper personalizado
        $line = get_model_selectable(Line::all());
        $category = get_model_selectable(Category::all());
        return view('admin.product.create',compact('line','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = new Product($request->all());
        $product->slug = Str::slug($product->name);
        $product->save();
        $product->details()->create($request->all());
        return redirect()->route('product.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $line = get_model_selectable(Line::all());
        $category = get_model_selectable(Category::all());
        Session::flash('product_id',$product->id);
        return view('admin.product.edit', compact('product','category','line'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $product->fill($request->all());
        $product->slug = Str::slug($product->name);
        $product->details()->update($request->only('cost_price','description'));
        $product->save();
        return redirect()->route('product.edit',$product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->details()->delete();
        $product->delete();
        return redirect()->route('product.index');
    }
}
