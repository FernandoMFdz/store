<?php

namespace App\Http\Controllers\Admin;


use App\Order;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware('role:director');
    }

    public function index()
    {
        $paid_orders = $this->getPaidOrders();
        $pending_orders = $this->getPendingOrders();
        $total_cash = $this->getTotalCashThisMonth();
        $total_new_clients = $this->getTotalClients();
        $total_cash_pending = $this->getTotalCashPendingThisMonth();
        if($total_cash == 0 && $total_cash_pending == 0 )
            $cash_percent = 0;
        else
            $cash_percent = ($total_cash*100)/($total_cash + $total_cash_pending);


        return view(
            'admin.dashboard.index',
            compact(
                'paid_orders',
                'pending_orders',
                'total_cash',
                'total_new_clients',
                'total_cash_pending',
                'cash_percent'
            )
        );
    }

    private function getPaidOrders()
    {
        return Order::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('status','=','Paid')
            ->get()
            ->count();
    }

    private function getPendingOrders()
    {
        return Order::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('status','=','Pending')
            ->get()
            ->count();
    }

    private function getTotalCashThisMonth()
    {
        $orders = Order::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('status','=','Paid')
            ->get();
        $total_cash = 0;
        foreach ($orders as $order){
                $total_cash += $order->total;
        }
        return $total_cash;
    }

    private function getTotalClients()
    {
        $users = User::where('created_at', '>=', Carbon::now()->startOfMonth())->get();
        $total= 0;
        foreach ($users as $user){
            if($user->rol == "cliente"){
                $total = $total+1;
            }
        }
        return $total;
    }
    private function getTotalCashPendingThisMonth()
    {
        $orders = Order::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('status','=','Pending')
            ->get();
        $total_cash = 0;
        foreach ($orders as $order){
            foreach ($order->details as $detail){
                $total_cash += $detail->quantity * $detail->product->sell_price;
            }
        }
        return $total_cash;
    }
}
