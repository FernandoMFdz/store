<?php

namespace App\Http\Controllers\Store;

use App\Product;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::getOneByName($slug);

        return view('store.product.show',compact('product'));

    }
}
