<?php

namespace App\Http\Controllers\Store;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category_name
     * @return \Illuminate\Http\Response
     */
    public function show($category_name)
    {
        $category = Category::getAllByName($category_name);
        if ($category === null) abort(404);
        return view('store.category.show',compact('category'));
    }
}
