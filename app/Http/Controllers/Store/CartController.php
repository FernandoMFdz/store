<?php

namespace App\Http\Controllers\Store;

use App\Order;
use App\OrderDetail;
use App\User;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cart()
    {
        $order = Order::getOrderByUser(auth()->user()->id,'Pending');
        $order_id = ($order)?$order->id:null;
        return view('store.cart.show',compact('order','order_id'));

    }

    public function delItem(OrderDetail $order_detail)
    {
        $order = $order_detail->order;
        if($order->details->count() == 1){
            $order_detail->delete();
            $order->delete();
        }else{
            $order_detail->delete();
        }
        return redirect()->back();
    }

    public function pay(Order $order)
    {
        //Conectar con paypal y realizar el pago
        $order->status = "Paid";
        $order->save();

        //redirigir a una vista de agradecimiento en un futuro
        return redirect()->route('home');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Product $product,Request $request)
    {
        $order = Order::getOrderByUser(auth()->user()->id,'Pending');

        if($order === null){
            $order = auth()->user()->orders()->create(['status'=>'Pending']);
        }
        $details = OrderDetail::getOrderDetailsByProduct($order->id,$product->id);
        if($details == null){
            if ($request->input('quantity') != 0){
                $order->details()->create(['quantity'=>$request->get('quantity'),'product_id' => $product->id]);
            }
        }else{
            $quantity = $details->quantity;
            $details->update(['quantity'=> $quantity+$request->get('quantity')]);
        }

        return redirect()->back();

    }

}
