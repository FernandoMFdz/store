<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $phone = "required|integer";
        $address = "required|min:4|max:255";
        $nif_cif = "required|min:4|max:255";
        $web = "required|url";
        $twitter = "required|min:4|max:255|url";
        $contact_name = "required|min:4|max:255";

        switch($this->method())
        {

            case 'POST':
            {
                return [
                    'name' => 'required|min:2|max:255|alpha_num|unique:brands,name',
                    'email' => 'required|email|unique:brand_details,email',
                    'phone' => $phone,
                    'address' => $address,
                    'nif_cif' => $nif_cif,
                    'web' => $web,
                    'twitter' => $twitter,
                    'facebook' => $twitter,
                    'contact_name' => $contact_name
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|unique:brands,name,'. Session::get('brand_id'),
                    'email' => 'required|email|unique:brand_details,email,'.Session::get('brand_details_id'),
                    'phone' => $phone,
                    'address' => $address,
                    'nif_cif' => $nif_cif,
                    'web' => $web,
                    'twitter' => $twitter,
                    'facebook' => $twitter,
                    'contact_name' => $contact_name
                ];
            }
        }
    }
}
