<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        switch($this->method())
        {

            case 'POST':
            {
                return [
                    'name' => 'required|min:2|max:255',
                    'email'      => 'required|email|unique:users,email',
                    'password'   => 'required',
                    'rol' => 'required|in:programador,director,administrador,personal_tienda,personal_salon,cliente'
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|min:2|max:255',
                    'email'      => 'required|unique:users,email,'. Session::get('user_id'),
                    'rol' => 'required|in:programador,director,administrador,personal_tienda,personal_salon,cliente'
                ];
            }
        }
    }
}
