<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'name' => 'required|min:4|max:255|unique:category,name',
                    'description' => 'required|min:4|max:255'
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|min:4|max:255|unique:category,name,'. Session::get('category_id'),
                    'description' => 'required|min:4|max:255'

                ];

            }
        }
    }
}
