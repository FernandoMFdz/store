<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       
        $brands = Line::all();
        $brand_help = "";
        foreach ($brands as $brand){
            $brand_help .= $brand->id.",";
        }
        $category = Category::all();
        $category_help = "";
        foreach ($categories as $category){
            $category_help .= $category->id.",";
        }

        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'name' => 'required|min:4|max:255|unique:products,name',
                    'sell_price' => 'required|numeric',
                    'discounted_price' => 'required|numeric',
                    'cost_price' => 'required|numeric',
                    'description' => 'required|min:4|max:255',
                    'line_id' => 'required|in:'.$brand_help,
                    'category_id' => 'required|in:'.$category_help
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|min:4|max:255|unique:products,name,'. Session::get('product_id'),
                    'sell_price' => 'required|numeric',
                    'discounted_price' => 'required|numeric',
                    'cost_price' => 'required|numeric',
                    'description' => 'required|min:4|max:255',
                    'line_id' => 'required|in:'.$brand_help,
                    'category_id' => 'required|in:'.$category_help
                ];

            }
        }
    }
}
