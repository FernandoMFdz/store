<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $brands = Brand::all();
        $brand_help = "";
        foreach ($brands as $brand){
            $brand_help .= $brand->id.",";
        }

        switch($this->method())
        {

            case 'POST':
            {
                return [
                    'name' => 'required|min:4|max:255|unique:lines,name',
                    'description' => 'required|min:4|max:255',
                    'brand_id' => "required|in:".$brand_help
                ];
            }
            case 'PUT':
            {
                return [

                    'name' => 'required|min:4|max:255|unique:lines,name,'. Session::get('line_id'),
                    'description' => 'required|min:4|max:255',
                    'brand_id' => "required|in:".$brand_help

                ];

            }
        }
    }
}
