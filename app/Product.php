<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'sell_price','discounted_price','line_id','category_id'
    ];

    public function getDiscountPercentAttribute()
    {
        return ($this->discounted_price * 100) / $this->sell_price;
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function details()
    {
        return $this->hasOne('App\ProductDetail');
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function line()
    {
        return $this->belongsTo('App\Line');
    }

    public function getDescriptionAttribute()
    {
        return $this->details->description;
    }

    public function getCostPriceAttribute()
    {
        return $this->details->cost_price;
    }

    public static function getOneByName($name)
    {
        $result = Product::select()->where('slug','=',$name)->get();
        if ($result->isEmpty()) return null;
        $product = Product::findOrFail($result[0]->id);
        return $product;
    }

    public function scopeName($query,$name)
    {
        if (trim($name) != '') {
            $query->where('name','LIKE',"%$name%");
        }
    }
}
