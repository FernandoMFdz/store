<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','rol'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeRol($query, $rol)
    {
        $roles = config('options.rol');
        if ($rol != '' && isset($roles[$rol])) {
            $query->where('rol', $rol);
        }
    }
}
