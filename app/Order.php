<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id','status'
    ];

    public function details()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public static function getOrderByUser($user,$status)
    {
        $query = Order::select('id')
            ->where('user_id','=',$user)
            ->where('status','=',$status)
            ->get();

        if ($query->isEmpty()) return null;
        $order = Order::findOrFail($query[0]->id);
        return $order;
    }

    public static function getOrderByStatus($status)
    {
        $query = Order::select()
            ->where('status','=',$status)
            ->get();

        if ($query->isEmpty()) return null;
        return $query;
    }

    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->details as $detail){
            $total += $detail->quantity*$detail->product->sell_price;
        }

        return $total;
    }

    public function scopeCode($query,$id)
    {
        if ($id != '') {
            $query->where('id','=',"$id");
        }

    }

    public function scopeStatus($query,$status)
    {
        if ($status != '' && isset( config('options.order_status')[$status])){
            $query->where('status','=',$status);
        }
    }
}
