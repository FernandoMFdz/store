<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id', 'quantity', 'product_id'
    ];


    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public static function getOrderDetailsByProduct($order,$product)
    {
        $query = OrderDetail::select()
            ->where('order_id','=',$order)
            ->where('product_id','=',$product)
            ->get();

        if ($query->isEmpty()) return null;
        $order = OrderDetail::findOrFail($query[0]->id);
        return $order;
    }
}
