<?php

/**
 *  Obtiene una consulta de un modelo y lo transforma en un array pensado para
 *  usarlo en LaravelCollective Form::select...
 *
 * @param Object \Illuminate\Database\Eloquent\Model
 * @author Fernando Meseguer Fernández
 * @return array
 *
 */
function get_model_selectable($obj)
{
    $array = [];
    foreach ($obj as $var) {
        $array[$var->id] = $var->name;
    }
    return $array;
}

function get_menu()
{
    $categories = \App\Category::all();
    $lis = "";

    foreach ($categories as $category) {
        $lis .= "<li><a href='".url('/')."/" . $category->slug . "'>$category->name</a></li>";
    }

    return $lis;
}
function get_cart_item_count()
{
    $order = \App\Order::getOrderByUser(auth()->user()->id,'Pending');
    return ($order)? $order->details->count():0;
}

function get_shopping_cart()
{
    $query = \App\Order::select('id')
        ->where('user_id', '=', auth()->user()->id)
        ->where('status','=','Pending')
        ->get();
    if ($query->isEmpty()) return "<li class='clearfix'> The shopping cart is empty </li><hr>";

    $order = \App\Order::findOrFail($query[0]->id);

    if ($order->details->count() === 0) return "<li class='clearfix'> The shopping cart is empty </li><hr>";

    $lis = "";

    foreach ($order->details as $detail) {
        $product = \App\Product::find($detail->product_id);
        $lis .= '
                 <!-- Item -->
                 <li class="clearfix">
                   <img src="'.url('/').'/img/products/missingpic.jpg"  alt="">
                   <div class="text">
                     <a class="title" href="#">' . $product->name . '</a>
                     <div class="details">' . $detail->quantity . ' x ' . $product->sell_price . '
                        <div class="btn-group">'.Form::open(['method' => 'DELETE', 'route' => ['cart.detail.delete',$detail->id]]).'
                          <button type="submit" class="btn btn-default" href="#"><i class="fa fa-trash"></i></button>
                          '.Form::close().'
                        </div>
                     </div>
                   </div>
                 </li>
                 <!-- /Item -->
        
        ';
        //$lis .= "<li><a href='#'>".$product->name." x ".$detail->quantity." unidades</a></li>";
    }

    return $lis;
}